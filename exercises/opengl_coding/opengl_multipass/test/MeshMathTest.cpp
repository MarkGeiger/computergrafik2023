#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

#include "../src/helpers/meshmath.h"

namespace
{
    class MeshMathTest : public ::testing::Test
    {

    protected:
        MeshMathTest() = default;

        ~MeshMathTest() override = default;

        void SetUp() override
        {
        }

        void TearDown() override
        {
        }

    };

    TEST_F(MeshMathTest, TestIntersection)
    {
        glm::vec2 p1(0,0);
        glm::vec2 p2(1,1);
        glm::vec2 c1(0,1);
        glm::vec2 c2(1,0);
        auto optIntersection = meshmath::intersectLineSegments(p1, p2, c1, c2);

        EXPECT_TRUE(optIntersection.has_value());
        auto val = optIntersection.value();

        ASSERT_FLOAT_EQ(val.x, 0.5);
        ASSERT_FLOAT_EQ(val.y, 0.5);

        p1 = glm::vec2 (1,0);
        p2 = glm::vec2 (1,1);
        c1 = glm::vec2 (0,0.5);
        c2 = glm::vec2 (2,0.5);
        optIntersection = meshmath::intersectLineSegments(p1, p2, c1, c2);

        EXPECT_TRUE(optIntersection.has_value());
        val = optIntersection.value();

        ASSERT_FLOAT_EQ(val.x, 1.0);
        ASSERT_FLOAT_EQ(val.y, 0.5);

        p1 = glm::vec2 (-1,0);
        p2 = glm::vec2 (-1,-1);
        c1 = glm::vec2 (0,-0.5);
        c2 = glm::vec2 (-2,-0.5);
        optIntersection = meshmath::intersectLineSegments(p1, p2, c1, c2);

        EXPECT_TRUE(optIntersection.has_value());
        val = optIntersection.value();

        ASSERT_FLOAT_EQ(val.x, -1.0);
        ASSERT_FLOAT_EQ(val.y, -0.5);
    }
}
