#version 430

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 texCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec4 vPos;
out vec3 vNormal;
out vec4 vColor;
out vec2 vTexCoord;

void main()
{
    gl_Position =  proj * view * model * vec4(position, 1.0f);
    vPos =  model * vec4(position, 1.0f);
    vNormal = normal;
    vColor = vec4(color, 1.0f);
    vTexCoord = texCoord;
}