#version 430

layout (location = 0) out vec4 fragColor;

uniform vec3 lightSource;

in vec4 vPos;
in vec3 vNormal;
in vec4 vColor;
in vec2 vTexCoord;

uniform sampler2D ourTexture;

void main()
{
    //if (texture(ourTexture, vTexCoord)[1]  > 0.5)
    //    discard;

    //vec3 vertexToLight = normalize(lightSource - vec3(vPos));
    //float diffuse = max(dot(vNormal, vertexToLight), 0.0f);
    float diffuse = 0;
    float ambient = 0.3f;
    vec3 color = vec3(vColor) * (diffuse + ambient);
    fragColor = texture(ourTexture, vTexCoord);
}