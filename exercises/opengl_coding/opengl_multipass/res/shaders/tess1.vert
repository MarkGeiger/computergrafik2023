#version 430

layout (location = 0) in float position;

uniform vec2 control_points0;
uniform vec2 control_points1;
uniform vec2 control_points2;
uniform vec2 control_points3;

uniform vec2 startNeu;

uniform vec2 param_scale;
uniform vec2 param_offset;

void main(void)
{
    float t = position;

    vec2 temp1 = mix(control_points0, control_points1, t);
    vec2 temp2 = mix(control_points1, control_points2, t);
    vec2 temp3 = mix(control_points2, control_points3, t);

    temp1 = mix(temp1, temp2, t);
    temp2 = mix(temp2, temp3, t);

    temp1 = mix(temp1, temp2, t);

    if (position < -0.5)
    {
        temp1 = startNeu;
    }

    temp1 = temp1 * param_scale + param_offset;
    temp1 = temp1 * vec2(2.0f,2.0f) + vec2(-1.0f,-1.0f);

    gl_Position = vec4(temp1, 0.0f, 1.0f);
}