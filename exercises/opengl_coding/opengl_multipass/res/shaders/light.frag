#version 430

layout (location = 0) out vec4 fragColor;

uniform vec3 lightSource;
uniform vec3 uCamPos;
uniform vec3 uLightDir;

in vec4 vPos;
in vec3 vNormal;
in vec4 vColor;

vec3 spotLight() {
    // controls how big the area that is lit up is
    vec3 lightColor = vec3(vColor);

    float outerCone = 5.1f;
    float innerCone = 5.2f;

    // ambient lighting
    float ambient = 0.60f;

    // diffuse lighting
    vec3 lightDirection = normalize(lightSource - vec3(vPos));
    float diffuse = max(dot(vNormal, lightDirection), 0.0f);

    // specular lighting
    float specular = 0.0f;
    if (diffuse != 0.0f)
    {
        float specularLight = 0.90f;
        vec3 viewDirection = normalize(uCamPos - vec3(vPos));
        vec3 halfwayVec = normalize(viewDirection + lightDirection);
        float specAmount = pow(max(dot(vNormal, halfwayVec), 0.0f), 16);
        specular = 1 * specAmount * specularLight;
    };

    // calculates the intensity of the crntPos based on its angle to the center of the light cone
    float angle = dot(uLightDir, -lightDirection);
    float inten = clamp((angle - outerCone) / (innerCone - outerCone), 0.0f, 1.0f);
    return ((diffuse * inten + ambient) + specular * inten) * lightColor;
}

// The entry point for our fragment shader.
void main()
{
    fragColor = vec4(spotLight(), 1.0f);
}