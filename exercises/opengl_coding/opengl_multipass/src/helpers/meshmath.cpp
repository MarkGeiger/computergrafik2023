#include "meshmath.h"

namespace meshmath
{
    std::optional<glm::vec2> intersectLineSegments(const glm::vec2 &p1, const glm::vec2 &p2, const glm::vec2 &c1, const glm::vec2 &c2)
    {
        // Geraden Gleichungen aufstellen
        // g1: x = p1 + t(p2 - p1)
        // g2: x = c1 + r(c2 - c1)

        // LGS aufstellen
        // t(p2-p1) - r(c2-c1) = c1 - p1

        // substitution
        auto u = p2 - p1;
        auto v = c2 - c1;
        auto z = c1 - p1;

        // replace minus r
        v.x = -v.x;
        v.y = -v.y;

        // solve LGS
        // ( ux vx | zx )
        // ( uy vy | zy )

        // uy needs to be zero, thus:
        if (u.y != 0)
        {
            if (u.x == 0)
            {
                u.x = u.x + u.y;
                v.x = v.x + v.y;
                z.x = z.x + z.y;
            }

            float n = u.y / u.x;
            // Z1 * n minus Z2.

            u.y = u.y - u.x * n;
            v.y = v.y - v.x * n;
            z.y = z.y - z.x * n;
        }

        assert(abs(u.y) < 1e-4);

        // now turn vy to 1, then we can solve r!
        z.y = z.y / v.y;
        v.y = v.y / v.y;

        if (v.y >= 1 + 1e-8)
        {
            assert(v.y < 1 + 1e-4);
        }

        float r = z.y;
        float t = (z.x - v.x * r) / u.x;

        if (r > 1e-4 && r < 1 - 1e-4 &&
            t > 1e-4 && t < 1 - 1e-4)
        {
            return std::optional<glm::vec2>{(c1 + r * (c2 - c1))};
        }
        return std::nullopt;
    }
}
