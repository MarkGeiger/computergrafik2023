#pragma once

#include "vec2.hpp"

#include <optional>

namespace meshmath
{
    std::optional<glm::vec2>
    intersectLineSegments(const glm::vec2 &p1, const glm::vec2 &p2, const glm::vec2 &c1, const glm::vec2 &c2);
}