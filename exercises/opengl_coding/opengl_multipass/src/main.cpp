#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <iostream>
#include <algorithm>
#include <utility>

#include "rendering/Shader.h"
#include "rendering/TextRenderer.h"
#include "rendering/Window.h"
#include "rendering/Camera.h"
#include "rendering/Grid.h"
#include "rendering/CoordinateSystem.h"
#include "rendering/ObjectRenderer.h"

#include "model/FlapTransform.h"
#include "model/Cube.h"

#include "model/BiCubicBezierSurface.h"
#include "model/CubicBezierCurve.h"
#include "model/TrimmedBezierSurface.h"

using namespace windowConstants;

Window mainWindow;
Camera camera;
Grid floorGrid;

TextRenderer *textRenderer;
Shader *shader = nullptr;
Shader *shaderLight = nullptr;
Shader *shaderFirst = nullptr;
Shader *shaderSecond = nullptr;

int fps = 0;
int fpsCounter = 20;
double timeLastFrame = 0;

GLuint renderedTexture;
GLuint FramebufferName = 0;

void loadContent()
{
    shader = new Shader("Basic.vert", "Basic.frag");
    shader->apply();

    shader->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shader->setUniformMatrix4fv("view", camera.getViewMatrix());
    shader->setUniformMatrix4fv("proj", camera.getProjectionMatrix());

    shaderLight = new Shader("light.vert", "light.frag");
    shaderLight->apply();

    shaderLight->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shaderLight->setUniformMatrix4fv("view", camera.getViewMatrix());
    shaderLight->setUniformMatrix4fv("proj", camera.getProjectionMatrix());

    shaderFirst = new Shader("tess1.vert", "tess1.frag");
    shaderSecond = new Shader("tess2.vert", "tess2.frag");

    shaderSecond->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shaderSecond->setUniformMatrix4fv("view", camera.getViewMatrix());
    shaderSecond->setUniformMatrix4fv("proj", camera.getProjectionMatrix());

    glGenFramebuffers(1, &FramebufferName);
    glGenTextures(1, &renderedTexture);
}

void drawTopMenu(double time)
{
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    fpsCounter--;
    if (fpsCounter == 0)
    {
        fpsCounter = 20;
        fps = static_cast<int>(1 / (time - timeLastFrame));
    }
    const float SCALEX = 2.0 / WINDOW_WIDTH;
    const float SCALEY = 2.0 / WINDOW_HEIGHT;
    textRenderer->renderText("FPS: " + std::to_string(fps), -1, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F1: Wireframe", -0.7, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F2: Show Grid", -0.3, 0.95, SCALEX / 2, SCALEY / 2);
    timeLastFrame = time;
}

void render(double time)
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shaderLight->apply();
    shaderLight->setUniform3fv("lightSource", glm::vec3(1.5 /* * glm::sin(time)*/, 5, 9));
    shaderLight->setUniform3fv("uCamPos", camera.getCamPosition());
    shaderLight->setUniform3fv("uLightDir", glm::vec3(0, -4, -4));

    if (mainWindow.isShowGrid())
    {
        floorGrid.drawGrid();
    }

    if (mainWindow.isWireframesEnabled())
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    shaderSecond->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shaderSecond->setUniformMatrix4fv("view", camera.getViewMatrix());
    shaderSecond->setUniformMatrix4fv("proj", camera.getProjectionMatrix());

    glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, renderedTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 768, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

    shader->apply();
    GLfloat m_vertex_buffer_data[] =
            {
                    0, 0, 0, 0, 1, 0,
                    0, 0, 1, 0, 1, 0,
                    0, 1, 0, 0, 1, 0,
                    0, 1, 1, 0, 1, 0
            };
    ObjectRenderer::drawShapePosColor(m_vertex_buffer_data, sizeof(m_vertex_buffer_data),
                                      GL_TRIANGLE_STRIP);

    GLfloat m_vertex_buffer_data2[] =
            {
                    0, 0, 0, 0, 1, 1,
                    0, 2, 0, 1, 1, 1,
                    2, 0, 0, 1, 0, 1,
                    2, 2, 0, 0, 0, 1
            };
    ObjectRenderer::drawShapePosColor(m_vertex_buffer_data2, sizeof(m_vertex_buffer_data2),
                                      GL_TRIANGLE_STRIP);

    // reset to screen rendering
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, windowConstants::WINDOW_WIDTH,
               windowConstants::WINDOW_HEIGHT); // Set the viewport to the size of the

    // draw rectangle
    Rectangle rectangle(2, 2, glm::vec3(0, 0, 0), glm::vec3(0, 1.0, 0));
    glBindTexture(GL_TEXTURE_2D, renderedTexture);
    rectangle.draw(shaderSecond);

    shader->apply();
    if (mainWindow.isShowGrid())
    {
        shader->setUniformMatrix4fv("model", glm::mat4(1.0f));
        CoordinateSystem::drawCoordinateSystem();
    }

    glDisable(GL_DEPTH_TEST);
    drawTopMenu(time);
    glEnable(GL_DEPTH_TEST);
}

void mainLoop()
{
    auto startTime = static_cast<float>(glfwGetTime());
    double newTime;

    /* Loop until the user closes the window */
    while (mainWindow.startFrame())
    {
        newTime = static_cast<float>(glfwGetTime()) - startTime;

        render(newTime);

        mainWindow.endFrame();
    }
}

int main()
{
    mainWindow.init();
    loadContent();
    floorGrid.setShader(*shaderLight);

    mainWindow.setShader(*shader, *shaderLight);
    mainWindow.setCamera(camera);

    textRenderer = new TextRenderer();

    // does not exit method until openGL window closed
    mainLoop();

    // cleanup
    glfwTerminate();
    delete shader;
    delete shaderLight;
    delete textRenderer;
    return 0;
}