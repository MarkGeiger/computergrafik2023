#pragma once

#include "BiCubicBezierSurface.h"
#include "CubicBezierCurve.h"

#include "vector"
#include "rendering/Shader.h"

class TrimmedBezierSurface
{
public:
    TrimmedBezierSurface(const BiCubicBezierSurface<glm::vec3> &surface,
                         const std::vector<std::vector<CubicBezierCurve<glm::vec2>>> &trimCurves);


    void draw(Shader &shaderFirst, Shader &shaderSecond, glm::mat4 mat);

private:
    void
    addTriangle(std::vector<GLfloat> &buffer, std::pair<glm::vec3, glm::vec3> &v1, std::pair<glm::vec3, glm::vec3> &v2,
                std::pair<glm::vec3, glm::vec3> &v3, glm::vec3 color) const;

    BiCubicBezierSurface<glm::vec3> m_surface;
    std::vector<std::vector<CubicBezierCurve<glm::vec2>>> m_trimCurves;
};
