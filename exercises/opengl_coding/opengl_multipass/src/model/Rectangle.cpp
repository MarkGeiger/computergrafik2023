
#include "Rectangle.h"
#include "rendering/Shader.h"
#include "rendering/ObjectRenderer.h"

#define STB_IMAGE_IMPLEMENTATION

#include "stb_image.h"
#include "helpers/RootDir.h"

Rectangle::Rectangle(float width, float heigth, glm::vec3 center, glm::vec3 color) :
        m_width(width), m_heigth(heigth), m_center(center), m_tras(glm::mat4(1.0f))
{
    auto n = getNormal();
    m_vertex_buffer_data[0] = -m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[1] = -m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[2] = m_center.z;
    m_vertex_buffer_data[3] = color.r;
    m_vertex_buffer_data[4] = color.g;
    m_vertex_buffer_data[5] = color.b;
    m_vertex_buffer_data[6] = n.x;
    m_vertex_buffer_data[7] = n.y;
    m_vertex_buffer_data[8] = n.z;
    m_vertex_buffer_data[9] = 0;
    m_vertex_buffer_data[10] = 0;

    m_vertex_buffer_data[11] = -m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[12] = m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[13] = m_center.z;
    m_vertex_buffer_data[14] = color.r;
    m_vertex_buffer_data[15] = color.g;
    m_vertex_buffer_data[16] = color.b;
    m_vertex_buffer_data[17] = n.x;
    m_vertex_buffer_data[18] = n.y;
    m_vertex_buffer_data[19] = n.z;
    m_vertex_buffer_data[20] = 0;
    m_vertex_buffer_data[21] = 1;

    m_vertex_buffer_data[22] = m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[23] = -m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[24] = m_center.z;
    m_vertex_buffer_data[25] = color.r;
    m_vertex_buffer_data[26] = color.g;
    m_vertex_buffer_data[27] = color.b;
    m_vertex_buffer_data[28] = n.x;
    m_vertex_buffer_data[29] = n.y;
    m_vertex_buffer_data[30] = n.z;
    m_vertex_buffer_data[31] = 1;
    m_vertex_buffer_data[32] = 0;

    m_vertex_buffer_data[33] = m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[34] = m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[35] = m_center.z;
    m_vertex_buffer_data[36] = color.r;
    m_vertex_buffer_data[37] = color.g;
    m_vertex_buffer_data[38] = color.b;
    m_vertex_buffer_data[39] = n.x;
    m_vertex_buffer_data[40] = n.y;
    m_vertex_buffer_data[41] = n.z;
    m_vertex_buffer_data[42] = 1;
    m_vertex_buffer_data[43] = 1;
}

void Rectangle::draw(Shader *pShader)
{
    pShader->apply();

    updateNormals();
    pShader->setUniformMatrix4fv("model", m_tras);
    ObjectRenderer::drawShapePosColorNormalTexCoord(m_vertex_buffer_data, sizeof(m_vertex_buffer_data),
                                                    GL_TRIANGLE_STRIP);
}

void Rectangle::rotate(float angle, glm::vec3 rotateAround)
{
    auto tras = m_tras;
    tras = glm::translate(tras, m_center);
    tras = glm::rotate(tras, glm::radians(angle), rotateAround);
    tras = glm::translate(tras, -m_center);
    m_tras = tras;
}

glm::vec3 Rectangle::transformRectangleToWorldCoordinates(glm::vec2 cubeCoordinates)
{
    float startX = -m_width / 2.0f + m_center.x;
    float startY = m_heigth / 2.0f + m_center.y;
    return m_tras *
           glm::vec4(startX + cubeCoordinates.x * m_width, startY - cubeCoordinates.y * m_heigth, m_center.z, 1.0f);
}

glm::vec3 Rectangle::getNormal()
{
    glm::vec3 v1 = transformRectangleToWorldCoordinates(glm::vec2(0, 0));
    glm::vec3 v2 = transformRectangleToWorldCoordinates(glm::vec2(1, 0));
    glm::vec3 v3 = transformRectangleToWorldCoordinates(glm::vec2(1, 1));
    auto v1v2 = glm::normalize(v2 - v1);
    auto v3v2 = glm::normalize(v2 - v3);
    return glm::normalize(glm::cross(v1v2, v3v2));
}

void Rectangle::updateNormals()
{
    auto n = getNormal();
    m_vertex_buffer_data[6] = n.x;
    m_vertex_buffer_data[7] = n.y;
    m_vertex_buffer_data[8] = n.z;

    m_vertex_buffer_data[17] = n.x;
    m_vertex_buffer_data[18] = n.y;
    m_vertex_buffer_data[19] = n.z;

    m_vertex_buffer_data[28] = n.x;
    m_vertex_buffer_data[29] = n.y;
    m_vertex_buffer_data[30] = n.z;

    m_vertex_buffer_data[39] = n.x;
    m_vertex_buffer_data[40] = n.y;
    m_vertex_buffer_data[41] = n.z;
}
glm::mat4 Rectangle::getTransform()
{
    return m_tras;
}
void Rectangle::setTransform(glm::mat4 transform)
{
    m_tras = transform;
}
