#include "TrimmedBezierSurface.h"
#include "rendering/ObjectRenderer.h"
#include "helpers/meshmath.h"

TrimmedBezierSurface::TrimmedBezierSurface(const BiCubicBezierSurface<glm::vec3> &surface,
                                           const std::vector<std::vector<CubicBezierCurve<glm::vec2>>> &trimCurves)
        : m_surface(surface), m_trimCurves(trimCurves)
{
}


void TrimmedBezierSurface::draw(Shader &shaderFirst, Shader &shaderSecond, glm::mat4 mat)
{
    shaderFirst.apply();

    for (auto curve: m_trimCurves)
    {
        shaderFirst.setUniform2fv("startNeu", curve[0].getControlPoints()[0]);
        shaderFirst.setUniform2fv("param_offset", glm::vec2(0.0f, 0.0f));
        shaderFirst.setUniform2fv("param_scale", glm::vec2(1.0f, 1.0f));
        for (auto curveSegment: curve)
        {
            shaderFirst.setUniform2fv("control_points0", curveSegment.getControlPoints()[0]);
            shaderFirst.setUniform2fv("control_points1", curveSegment.getControlPoints()[1]);
            shaderFirst.setUniform2fv("control_points2", curveSegment.getControlPoints()[2]);
            shaderFirst.setUniform2fv("control_points3", curveSegment.getControlPoints()[3]);

            std::vector<GLfloat> buffer;
            float stepSize = 0.125f / 2.0f;
            float i = 0;
            int count = 0;
            while (i <= 1.0f)
            {
                if (count % 3 == 0)
                {
                    buffer.emplace_back(-1.0f);
                } else
                {
                    buffer.emplace_back(i);
                    if (count % 3 != 2)
                    {
                        i = i + stepSize;
                    }
                }
                count++;
            }
            ObjectRenderer::drawShapePos1D(&buffer.front(), buffer.size() * sizeof(GLfloat), GL_TRIANGLES);
        }
    }



    /*
    float stepSize = 0.125f / 2.0f;
    std::vector<GLfloat> buffer;
    std::vector<glm::vec2> cornerPoints;
    for (float i = 0; i <= 1.0f - stepSize; i = i + stepSize)
    {
        for (float t = 0; t <= 1.0f - stepSize; t = t + stepSize)
        {
            auto v1 = m_surface.eval(i, t);
            auto v2 = m_surface.eval(i, t + stepSize);
            auto v3 = m_surface.eval(i + stepSize, t);
            auto v4 = m_surface.eval(i + stepSize, t + stepSize);

            // t1
            addTriangle(buffer, v1, v2, v3, glm::vec3(1, 0, 0));

            // t2
            addTriangle(buffer, v4, v2, v3, glm::vec3(1, 0, 0));
        }
    }
    ObjectRenderer::drawShapePosColorNormal(&buffer.front(), buffer.size() * sizeof(GLfloat), GL_TRIANGLES);
    */
}
void TrimmedBezierSurface::addTriangle(std::vector<GLfloat> &buffer, std::pair<glm::vec3, glm::vec3> &v1,
                                       std::pair<glm::vec3, glm::vec3> &v2,
                                       std::pair<glm::vec3, glm::vec3> &v3, glm::vec3 color) const
{
    buffer.emplace_back(v1.first.x);
    buffer.emplace_back(v1.first.y);
    buffer.emplace_back(v1.first.z);
    buffer.emplace_back(color.x);
    buffer.emplace_back(color.y);
    buffer.emplace_back(color.z);
    buffer.emplace_back(v1.second.x);
    buffer.emplace_back(v1.second.y);
    buffer.emplace_back(v1.second.z);
    buffer.emplace_back(v2.first.x);
    buffer.emplace_back(v2.first.y);
    buffer.emplace_back(v2.first.z);
    buffer.emplace_back(color.x);
    buffer.emplace_back(color.y);
    buffer.emplace_back(color.z);
    buffer.emplace_back(v2.second.x);
    buffer.emplace_back(v2.second.y);
    buffer.emplace_back(v2.second.z);
    buffer.emplace_back(v3.first.x);
    buffer.emplace_back(v3.first.y);
    buffer.emplace_back(v3.first.z);
    buffer.emplace_back(color.x);
    buffer.emplace_back(color.y);
    buffer.emplace_back(color.z);
    buffer.emplace_back(v3.second.x);
    buffer.emplace_back(v3.second.y);
    buffer.emplace_back(v3.second.z);
}