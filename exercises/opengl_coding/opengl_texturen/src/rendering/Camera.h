#pragma once

#define GLFW_INCLUDE_NONE

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Constants.h"

class Camera
{
public:

    [[nodiscard]] glm::vec3 getCamPosition() const;

    [[nodiscard]] glm::vec3 getCamLookAt() const;

    [[nodiscard]] glm::vec3 getCamUp() const;

    void setCamPosition(const glm::vec3 &mCamPosition);

    void setCamLookAt(const glm::vec3 &mCamLookAt);

    void setCamUp(const glm::vec3 &mCamUp);

    [[nodiscard]] glm::mat4 getViewMatrix() const;

    [[nodiscard]] glm::mat4 getProjectionMatrix() const;

    void makeNewProjectionMatrix(int w, int h);

    void setViewMatrix(glm::mat4 viewMatrix);

private:
    glm::vec3 m_camPosition = glm::vec3(1.0f, 1.0f, 2.5f);
    glm::vec3 m_camLookAt = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 m_camUp = glm::vec3(0.0f, 1.0f, 0.0f);

    glm::mat4 view_matrix = glm::lookAt(m_camPosition, m_camLookAt, m_camUp);
    glm::mat4 projection_matrix = glm::perspectiveFov(glm::radians(90.0f),
                                                      windowConstants::WINDOW_WIDTH,
                                                      windowConstants::WINDOW_HEIGHT,
                                                      0.1f,
                                                      30.0f);
};