#pragma once

#define GLFW_INCLUDE_NONE

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class CoordinateSystem
{
public:
    CoordinateSystem() = delete;

    static void drawCoordinateSystem();
};