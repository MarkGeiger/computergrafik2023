#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "rendering/Shader.h"

class Rectangle {

public:
    Rectangle(float width, float heigth, glm::vec3 center, glm::vec3 color);

    void rotate(float angle, glm::vec3 rotateAround);
    void draw(Shader *pShader);
    glm::vec3 getNormal();

    glm::vec3 transformRectangleToWorldCoordinates(glm::vec2 cubeCoordinates );

    void setTransform(glm::mat4 transform);
    glm::mat4 getTransform();
private:
    void updateNormals();

    float m_width;
    float m_heigth;
    glm::vec3 m_center;
    glm::mat4 m_tras;
    GLfloat m_vertex_buffer_data[44]{};
    unsigned int m_texture;
};