
#include "Rectangle.h"
#include "rendering/Shader.h"

Rectangle::Rectangle(float width, float heigth, glm::vec3 center) :
        m_width(width), m_heigth(heigth), m_center(center), m_tras(glm::mat4(1.0f))
{
    m_vertex_buffer_data[0] = -m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[1] = -m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[2] = m_center.z;
    m_vertex_buffer_data[3] = 1.0f;
    m_vertex_buffer_data[4] = 0.0f;
    m_vertex_buffer_data[5] = 0.0f;

    m_vertex_buffer_data[6] = -m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[7] = m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[8] = m_center.z;
    m_vertex_buffer_data[9] = 1.0f;
    m_vertex_buffer_data[10] = 0.0f;
    m_vertex_buffer_data[11] = 1.0f;

    m_vertex_buffer_data[12] = m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[13] = -m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[14] = m_center.z;
    m_vertex_buffer_data[15] = 1.0f;
    m_vertex_buffer_data[16] = 1.0f;
    m_vertex_buffer_data[17] = 0.0f;

    m_vertex_buffer_data[18] = m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[19] = m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[20] = m_center.z;
    m_vertex_buffer_data[21] = 0.0f;
    m_vertex_buffer_data[22] = 1.0f;
    m_vertex_buffer_data[23] = 0.0f;
}

void drawShape2(const GLfloat *m_vertex_buffer_data, unsigned long size, uint16_t type)
{
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, static_cast<long>(size), m_vertex_buffer_data, GL_STATIC_DRAW);

    // first attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) nullptr);
    glEnableVertexAttribArray(0);
    // second attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) (3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glDrawArrays(type, 0, static_cast<int>(size / sizeof(float) / 6));
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void Rectangle::draw(Shader *pShader)
{
    pShader->setUniformMatrix4fv("model", m_tras);
    drawShape2(m_vertex_buffer_data, sizeof(m_vertex_buffer_data), GL_TRIANGLE_STRIP);
}

void Rectangle::setTransform(glm::mat4 trans)
{
    m_tras = trans;
}