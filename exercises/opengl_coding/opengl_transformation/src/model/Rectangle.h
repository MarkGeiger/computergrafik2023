#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "rendering/Shader.h"

class Rectangle {

public:
    Rectangle(float width, float heigth, glm::vec3 center);
    void draw(Shader *pShader);
    void setTransform(glm::mat4 trans);

private:
    float m_width;
    float m_heigth;
    glm::vec3 m_center;
    glm::mat4 m_tras;
    GLfloat m_vertex_buffer_data[24];
};