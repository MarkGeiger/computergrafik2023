#pragma once

#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <ft2build.h>
#include FT_FREETYPE_H


class TextRenderer {

public:
    TextRenderer();

    ~TextRenderer();

    void renderText(const std::string &str, float x, float y, float sx, float sy);

private:
    GLuint texture{0}, sampler{0};
    GLuint vbo{0}, vao{0};
    GLuint vs{0}, fs{0}, program{0};
    FT_Library ft_lib{nullptr};
    FT_Face face{nullptr};
};
