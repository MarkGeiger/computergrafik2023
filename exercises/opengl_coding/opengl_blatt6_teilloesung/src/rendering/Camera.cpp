#include "Camera.h"

glm::mat4 Camera::getViewMatrix() const
{
    return view_matrix;
}

glm::mat4 Camera::getProjectionMatrix() const
{
    return projection_matrix;
}

void Camera::setCamPosition(const glm::vec3 &mCamPosition)
{
    m_camPosition = mCamPosition;
}

void Camera::setCamLookAt(const glm::vec3 &mCamLookAt)
{
    m_camLookAt = mCamLookAt;
}

void Camera::setCamUp(const glm::vec3 &mCamUp)
{
    m_camUp = mCamUp;
}

glm::vec3 Camera::getCamPosition() const
{
    return m_camPosition;
}

glm::vec3 Camera::getCamLookAt() const
{
    return m_camLookAt;
}

glm::vec3 Camera::getCamUp() const
{
    return m_camUp;
}

void Camera::makeNewProjectionMatrix(int w, int h)
{
    projection_matrix = glm::perspectiveFov(glm::radians(90.0f), float(w), float(h), 0.1f, 30.0f);
}

void Camera::setViewMatrix(glm::mat4 viewMatrix)
{
    view_matrix = viewMatrix;
}
