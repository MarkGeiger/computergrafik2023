#include "TrimmedBezierSurface.h"
#include <vector>
#include "gtc/matrix_transform.hpp"
#include "rendering/ObjectRenderer.h"

TrimmedBezierSurface::TrimmedBezierSurface(const BiCubicBezierSurface<glm::vec3> &surface,
                                           const std::vector<std::vector<CubicBezierCurve<glm::vec2>>> &trimCurve)
        : m_surface(surface),
          m_trimCurves((trimCurve)) {
}

void TrimmedBezierSurface::draw(Shader &shaderSurface, Shader &shaderCurve) {

    shaderSurface.apply();
    shaderSurface.setUniformMatrix4fv("model", glm::mat4(1.0f) *
                                               glm::rotate(glm::mat4(1.0f), glm::radians(30.0f), glm::vec3(1, 0, 0)));
    float stepSize = 0.125f / 4.0f;
    std::vector<GLfloat> buffer;
    for (float i = 0; i <= 1.0f - stepSize; i = i + stepSize) {
        for (float t = 0; t <= 1.0f - stepSize; t = t + stepSize) {
            auto v1 = m_surface.eval(i, t);
            auto v2 = m_surface.eval(i, t + stepSize);
            auto v3 = m_surface.eval(i + stepSize, t);
            auto v4 = m_surface.eval(i + stepSize, t + stepSize);
            // t1
            buffer.emplace_back(v1.first.x);
            buffer.emplace_back(v1.first.y);
            buffer.emplace_back(v1.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v1.second.x);
            buffer.emplace_back(v1.second.y);
            buffer.emplace_back(v1.second.z);
            buffer.emplace_back(v2.first.x);
            buffer.emplace_back(v2.first.y);
            buffer.emplace_back(v2.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v2.second.x);
            buffer.emplace_back(v2.second.y);
            buffer.emplace_back(v2.second.z);
            buffer.emplace_back(v3.first.x);
            buffer.emplace_back(v3.first.y);
            buffer.emplace_back(v3.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v3.second.x);
            buffer.emplace_back(v3.second.y);
            buffer.emplace_back(v3.second.z);

            // t2
            buffer.emplace_back(v4.first.x);
            buffer.emplace_back(v4.first.y);
            buffer.emplace_back(v4.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v4.second.x);
            buffer.emplace_back(v4.second.y);
            buffer.emplace_back(v4.second.z);
            buffer.emplace_back(v2.first.x);
            buffer.emplace_back(v2.first.y);
            buffer.emplace_back(v2.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v2.second.x);
            buffer.emplace_back(v2.second.y);
            buffer.emplace_back(v2.second.z);
            buffer.emplace_back(v3.first.x);
            buffer.emplace_back(v3.first.y);
            buffer.emplace_back(v3.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v3.second.x);
            buffer.emplace_back(v3.second.y);
            buffer.emplace_back(v3.second.z);
        }
    }
    ObjectRenderer::drawShapePosColorNormal(&buffer.front(), buffer.size() * sizeof(GLfloat), GL_TRIANGLES);

    shaderCurve.apply();
    //e.setUniformMatrix4fv("model", glm::mat4(1.0f));
    shaderCurve.setUniformMatrix4fv("model", glm::mat4(1.0f) *
                                             glm::rotate(glm::mat4(1.0f), glm::radians(30.0f), glm::vec3(1, 0, 0)));

    for (const auto &loop: m_trimCurves) {
        for (auto curveSegment: loop) {
            std::vector<GLfloat> bufferCurve;
            for (float t = 0; t <= 1.0f - stepSize; t = t + stepSize) {
                auto p1 = curveSegment.eval(t);
                auto p2 = curveSegment.eval(t + stepSize);

                auto e1 = m_surface.eval(p1.x, p1.y);
                auto e2 = m_surface.eval(p2.x, p2.y);

                bufferCurve.emplace_back(e1.first.x);
                bufferCurve.emplace_back(e1.first.y);
                bufferCurve.emplace_back(e1.first.z);
                bufferCurve.emplace_back(0.0f);
                bufferCurve.emplace_back(0);
                bufferCurve.emplace_back(1.0f);
                bufferCurve.emplace_back(e2.first.x);
                bufferCurve.emplace_back(e2.first.y);
                bufferCurve.emplace_back(e2.first.z);
                bufferCurve.emplace_back(0.0f);
                bufferCurve.emplace_back(0);
                bufferCurve.emplace_back(1.0f);
            }
            ObjectRenderer::drawShapePosColor(&bufferCurve.front(), bufferCurve.size() * sizeof(GLfloat), GL_LINES);
        }
    }

}
