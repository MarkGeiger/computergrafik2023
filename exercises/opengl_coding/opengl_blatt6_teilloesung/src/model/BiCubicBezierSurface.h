#pragma once

#include "detail/type_mat.hpp"
#include "detail/type_mat4x4.hpp"

#include <utility>

template<class T>
class BiCubicBezierSurface
{
public:
    BiCubicBezierSurface(
            T p1, T p2, T p3, T p4,
            T p5, T p6, T p7, T p8,
            T p9, T p10, T p11, T p12,
            T p13, T p14, T p15, T p16
    );

    std::pair<T, T> eval(float u, float v);

private:
    T m_controlPoints[4][4];
};

template<class T>
std::pair<T, T> BiCubicBezierSurface<T>::eval(float u, float v)
{
    T cp[4][4];
    cp[0][0] = m_controlPoints[0][0];
    cp[0][1] = m_controlPoints[0][1];
    cp[0][2] = m_controlPoints[0][2];
    cp[0][3] = m_controlPoints[0][3];
    cp[1][0] = m_controlPoints[1][0];
    cp[1][1] = m_controlPoints[1][1];
    cp[1][2] = m_controlPoints[1][2];
    cp[1][3] = m_controlPoints[1][3];
    cp[2][0] = m_controlPoints[2][0];
    cp[2][1] = m_controlPoints[2][1];
    cp[2][2] = m_controlPoints[2][2];
    cp[2][3] = m_controlPoints[2][3];
    cp[3][0] = m_controlPoints[3][0];
    cp[3][1] = m_controlPoints[3][1];
    cp[3][2] = m_controlPoints[3][2];
    cp[3][3] = m_controlPoints[3][3];

    // reduziere Dimension u

    cp[0][0] = glm::mix(cp[0][0], cp[0][1], v);
    cp[0][1] = glm::mix(cp[0][1], cp[0][2], v);
    cp[0][2] = glm::mix(cp[0][2], cp[0][3], v);

    cp[1][0] = glm::mix(cp[1][0], cp[1][1], v);
    cp[1][1] = glm::mix(cp[1][1], cp[1][2], v);
    cp[1][2] = glm::mix(cp[1][2], cp[1][3], v);

    cp[2][0] = glm::mix(cp[2][0], cp[2][1], v);
    cp[2][1] = glm::mix(cp[2][1], cp[2][2], v);
    cp[2][2] = glm::mix(cp[2][2], cp[2][3], v);

    cp[3][0] = glm::mix(cp[3][0], cp[3][1], v);
    cp[3][1] = glm::mix(cp[3][1], cp[3][2], v);
    cp[3][2] = glm::mix(cp[3][2], cp[3][3], v);

    // reduziere Dimension u

    cp[0][0] = glm::mix(cp[0][0], cp[0][1], v);
    cp[0][1] = glm::mix(cp[0][1], cp[0][2], v);

    cp[1][0] = glm::mix(cp[1][0], cp[1][1], v);
    cp[1][1] = glm::mix(cp[1][1], cp[1][2], v);

    cp[2][0] = glm::mix(cp[2][0], cp[2][1], v);
    cp[2][1] = glm::mix(cp[2][1], cp[2][2], v);

    cp[3][0] = glm::mix(cp[3][0], cp[3][1], v);
    cp[3][1] = glm::mix(cp[3][1], cp[3][2], v);

    // reduziere Dimension v

    cp[0][0] = glm::mix(cp[0][0], cp[1][0], u);
    cp[1][0] = glm::mix(cp[1][0], cp[2][0], u);
    cp[2][0] = glm::mix(cp[2][0], cp[3][0], u);

    cp[0][1] = glm::mix(cp[0][1], cp[1][1], u);
    cp[1][1] = glm::mix(cp[1][1], cp[2][1], u);
    cp[2][1] = glm::mix(cp[2][1], cp[3][1], u);

    // reduziere Dimension v

    cp[0][0] = glm::mix(cp[0][0], cp[1][0], u);
    cp[1][0] = glm::mix(cp[1][0], cp[2][0], u);

    cp[0][1] = glm::mix(cp[0][1], cp[1][1], u);
    cp[1][1] = glm::mix(cp[1][1], cp[2][1], u);

    T dirV1 = cp[1][0] - cp[0][0];
    T dirU1 = cp[0][1] - cp[0][0];

    T dirV2 = cp[1][1] - cp[0][1];
    T dirU2 = cp[1][1] - cp[1][0];

    float au = glm::max(1e-4f, glm::min(1e-4f, u));
    float av = glm::max(1e-4f, glm::min(1e-4f, v));
    T dirV = mix(dirV1, dirV2, au);
    T dirU = mix(dirU1, dirU2, av);

    T normal = glm::cross(dirV, dirU);

    // reduziere Dimension v

    cp[0][0] = glm::mix(cp[0][0], cp[1][0], u);
    cp[0][1] = glm::mix(cp[0][1], cp[1][1], u);

    // reduziere Dimension u

    cp[0][0] = glm::mix(cp[0][0], cp[0][1], v);

    return std::pair<T, T>(cp[0][0], normal);
}

template<class T>
BiCubicBezierSurface<T>::BiCubicBezierSurface(T p1, T p2, T p3, T p4, T p5, T p6, T p7, T p8, T p9, T p10, T p11, T p12,
                                              T p13, T p14, T p15, T p16)
{
    m_controlPoints[0][0] = p1;
    m_controlPoints[0][1] = p2;
    m_controlPoints[0][2] = p3;
    m_controlPoints[0][3] = p4;
    m_controlPoints[1][0] = p5;
    m_controlPoints[1][1] = p6;
    m_controlPoints[1][2] = p7;
    m_controlPoints[1][3] = p8;
    m_controlPoints[2][0] = p9;
    m_controlPoints[2][1] = p10;
    m_controlPoints[2][2] = p11;
    m_controlPoints[2][3] = p12;
    m_controlPoints[3][0] = p13;
    m_controlPoints[3][1] = p14;
    m_controlPoints[3][2] = p15;
    m_controlPoints[3][3] = p16;
}

