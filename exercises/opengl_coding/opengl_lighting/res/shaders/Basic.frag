#version 430

layout(location = 0) out vec4 fragColor;

uniform vec3 uLightPos;
uniform vec3 uLightDir;

in vec3 vPosition;
in vec3 vColor;
in vec3 vNormal;
in vec3 vCamPos;

vec3 spotLight() {
    // controls how big the area that is lit up is
    vec3 lightColor = vColor;

    float outerCone = 1.2f;
    float innerCone = 1.25f;

    // ambient lighting
    float ambient = 0.20f;

    // diffuse lighting
    vec3 lightDirection = normalize(uLightPos - vPosition);
    float diffuse = max(dot(vNormal, lightDirection), 0.0f);
    float diffuseI = max(dot(-vNormal, lightDirection), 0.0f);
    diffuse = max(diffuse, diffuseI);

    // specular lighting
    float specular = 0.0f;
    if (diffuse != 0.0f)
    {
        float specularLight = 0.90f;
        vec3 viewDirection = normalize(vCamPos - vPosition);
        vec3 halfwayVec = normalize(viewDirection + lightDirection);
        float specAmount = pow(max(dot(vNormal, halfwayVec), 0.0f), 16);
        float specAmount1 = pow(max(dot(-vNormal, halfwayVec), 0.0f), 16);
        specAmount = max(specAmount, specAmount1);
        specular = 3 * specAmount * specularLight;
    };

    // calculates the intensity of the crntPos based on its angle to the center of the light cone
    float angle = dot(uLightDir, -lightDirection);
    float inten = clamp((angle - outerCone) / (innerCone - outerCone), 0.0f, 1.0f);
    return ((diffuse * inten + ambient) + specular * inten) * lightColor;
}

// The entry point for our fragment shader.
void main()
{
    fragColor = vec4(spotLight(), 1.0f);
}