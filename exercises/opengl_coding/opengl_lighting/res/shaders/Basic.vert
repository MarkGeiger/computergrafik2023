#version 430

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec3 normal;
uniform vec3 camPos;

out vec3 vColor;
out vec3 vPosition;
out vec3 vNormal;
out vec3 vCamPos;

void main()
{
	vCamPos = camPos;
	gl_Position =  proj * view * model * vec4(position, 1.0f);
	vPosition = vec3(model * vec4(position, 1.0f));
	vNormal = normal;
	vColor = color;
}