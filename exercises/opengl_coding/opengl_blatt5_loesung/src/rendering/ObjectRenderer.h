#pragma once

#define GLFW_INCLUDE_NONE

#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace ObjectRenderer
{
    void drawShapePos(const GLfloat *g_vertex_buffer_data, unsigned long size, uint16_t type);

    void drawShapePosColor(const GLfloat *g_vertex_buffer_data, unsigned long size, uint16_t type);

    void drawShapePosColorNormal(const GLfloat *g_vertex_buffer_data, unsigned long size, uint16_t type);
}