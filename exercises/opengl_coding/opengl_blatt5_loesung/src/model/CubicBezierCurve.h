#pragma once

template<class T>
class CubicBezierCurve
{
public:
    CubicBezierCurve(T p1, T p2, T p3, T p4);
    T eval(float t);
private:
    T m_controlPoints[4];
};

template<class T>
T CubicBezierCurve<T>::eval(float t)
{
    T tmp[4];
    for (int i = 0; i < 4; i++)
    {
        tmp[i] = m_controlPoints[i];
    }

    for (int k = 1; k <= 3; k++)
    {
        for (int i = 0; i <= 3 - k; i++)
        {
            tmp[i] = (1 - t) * tmp[i] + t * tmp[i + 1];
        }
    }
    return tmp[0];
}

template<class T>
CubicBezierCurve<T>::CubicBezierCurve(T p1, T p2, T p3, T p4)
{
    m_controlPoints[0] = p1;
    m_controlPoints[1] = p2;
    m_controlPoints[2] = p3;
    m_controlPoints[3] = p4;
}
