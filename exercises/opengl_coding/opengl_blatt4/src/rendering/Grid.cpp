#include "Grid.h"
#include "ObjectRenderer.h"

#include <cstdint>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace grid;

Grid::Grid()
{
    for (uint8_t i = 0; i < gridSize; i++)
    {
        for (uint8_t j = 0; j < gridSize; j++)
        {
            uint8_t sizeOfOneVertex = 6;
            int index = (i * gridSize + j) * sizeOfOneVertex * 4;
            m_vertex_buffer_data_grid[index] = static_cast<float>(j) * gridStep; // x
            m_vertex_buffer_data_grid[index + 1] = 0; // y
            m_vertex_buffer_data_grid[index + 2] = static_cast<float>(i) * gridStep; // z
            m_vertex_buffer_data_grid[index + 3] = 0.0f; // r
            m_vertex_buffer_data_grid[index + 4] = 0.8f; // g
            m_vertex_buffer_data_grid[index + 5] = 0.0f; // b

            m_vertex_buffer_data_grid[index + 6] = static_cast<float>(j + 1) * gridStep; // x
            m_vertex_buffer_data_grid[index + 7] = 0; // y
            m_vertex_buffer_data_grid[index + 8] = static_cast<float>(i) * gridStep; // z
            m_vertex_buffer_data_grid[index + 9] = 0.0f; // r
            m_vertex_buffer_data_grid[index + 10] = 0.8f; // g
            m_vertex_buffer_data_grid[index + 11] = 0.0f; // b

            m_vertex_buffer_data_grid[index + 12] = static_cast<float>(j + 1) * gridStep; // x
            m_vertex_buffer_data_grid[index + 13] = 0; // y
            m_vertex_buffer_data_grid[index + 14] = static_cast<float>(i) * gridStep; // z
            m_vertex_buffer_data_grid[index + 15] = 0.0f; // r
            m_vertex_buffer_data_grid[index + 16] = 0.8f; // g
            m_vertex_buffer_data_grid[index + 17] = 0.0f; // b

            m_vertex_buffer_data_grid[index + 18] = static_cast<float>(j + 1) * gridStep; // x
            m_vertex_buffer_data_grid[index + 19] = 0; // y
            m_vertex_buffer_data_grid[index + 20] = static_cast<float>(i + 1) * gridStep; // z
            m_vertex_buffer_data_grid[index + 21] = 0.0f; // r
            m_vertex_buffer_data_grid[index + 22] = 0.8f; // g
            m_vertex_buffer_data_grid[index + 23] = 0.0f; // b
        }
    }
    uint8_t sizeOfOneVertex = 6;
    int index = ((gridSize - 1) * gridSize + gridSize) * sizeOfOneVertex * 4;
    m_vertex_buffer_data_grid[index] = static_cast<float>(0) * gridStep; // x
    m_vertex_buffer_data_grid[index + 1] = 0; // y
    m_vertex_buffer_data_grid[index + 2] = static_cast<float>(0) * gridStep; // z
    m_vertex_buffer_data_grid[index + 3] = 0.0f; // r
    m_vertex_buffer_data_grid[index + 4] = 0.8f; // g
    m_vertex_buffer_data_grid[index + 5] = 0.0f; // b

    m_vertex_buffer_data_grid[index + 6] = static_cast<float>(0) * gridStep; // x
    m_vertex_buffer_data_grid[index + 7] = 0; // y
    m_vertex_buffer_data_grid[index + 8] = static_cast<float>(gridSize) * gridStep; // z
    m_vertex_buffer_data_grid[index + 9] = 0.0f; // r
    m_vertex_buffer_data_grid[index + 10] = 0.8f; // g
    m_vertex_buffer_data_grid[index + 11] = 0.0f; // b

    m_vertex_buffer_data_grid[index + 12] = static_cast<float>(0) * gridStep; // x
    m_vertex_buffer_data_grid[index + 13] = 0; // y
    m_vertex_buffer_data_grid[index + 14] = static_cast<float>(gridSize) * gridStep; // z
    m_vertex_buffer_data_grid[index + 15] = 0.0f; // r
    m_vertex_buffer_data_grid[index + 16] = 0.8f; // g
    m_vertex_buffer_data_grid[index + 17] = 0.0f; // b

    m_vertex_buffer_data_grid[index + 18] = static_cast<float>(gridSize) * gridStep; // x
    m_vertex_buffer_data_grid[index + 19] = 0; // y
    m_vertex_buffer_data_grid[index + 20] = static_cast<float>(gridSize) * gridStep; // z
    m_vertex_buffer_data_grid[index + 21] = 0.0f; // r
    m_vertex_buffer_data_grid[index + 22] = 0.8f; // g
    m_vertex_buffer_data_grid[index + 23] = 0.0f; // b
}

void Grid::drawGrid()
{
    glm::mat4 trans = glm::translate(glm::mat4(1.0f),
                                     glm::vec3(-gridStep * gridSize * 0.5, -0.01, -gridStep * gridSize * 0.5));
    if (m_shader != nullptr)
    {
        m_shader->setUniformMatrix4fv("model", trans);
    }
    ObjectRenderer::drawShapePosColor(m_vertex_buffer_data_grid, sizeof(m_vertex_buffer_data_grid), GL_LINES);
}

void Grid::setShader(Shader &shader)
{
    m_shader = &shader;
}
