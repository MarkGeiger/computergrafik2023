#pragma once

#include "Constants.h"
#include "Shader.h"

class Grid
{
public:
    Grid();

    void setShader(Shader &shader);
    void drawGrid();

private:
    Shader *m_shader{nullptr};
    GLfloat m_vertex_buffer_data_grid[(grid::gridSize * grid::gridSize + 2) * 6 * 4] = {}; // 2 extra liens for border
};