#pragma once

#include <cstdint>

namespace windowConstants
{
    constexpr float WINDOW_WIDTH = 1024;
    constexpr float WINDOW_HEIGHT = 768;
}

namespace grid
{
    constexpr float gridStep = 0.5f;
    constexpr uint8_t gridSize = 50;
}
