#include "FlapTransform.h"

using namespace glm;

glm::mat4 FlapTransform::transform(vec3 hingePoint1, vec3 hingePoint2,
                                   vec3 hingePoint1s, vec3 hingePoint2s,
                                   float rotationAlpha)
{
    // four points, defining HingeLine before and after the translation.
    vec4 p1(hingePoint1, 1.0f);
    vec4 p2(hingePoint2, 1.0f);
    vec4 p1s(hingePoint1s, 1.0f);
    vec4 p2s(hingePoint2s, 1.0f);

    // those three vectors define the local coordinate System.
    vec4 exV = (p2 - p1);
    exV = normalize(exV);
    vec4 eyV;
    if (exV.y == 0)
    {
        eyV = vec4(2, 1 , -(2*exV.x + exV.y)/exV.z, 1.0f);
    } else {
        eyV = vec4(1, -(exV.x / exV.y), 0, 1.0f);
    }
    eyV = normalize(eyV);
   // vec4 ezV(-(exV.x * exV.z) / (exV.x * exV.x + exV.y * exV.y),
   //          -(exV.y * exV.z) / (exV.x * exV.x + exV.y * exV.y), 1, 1.0f);
    vec4 ezV = vec4(glm::cross(vec3(exV), vec3(eyV)),0);
    ezV = normalize(ezV);

    // translation
    vec4 t = p1s - p1;

    vec4 outerNewT = p2s - p1s;
    float p2Cx = dot(outerNewT, exV);
    float p2Cy = dot(outerNewT, eyV);
    float p2Cz = dot(outerNewT, ezV);
    vec4 transformedOuterOld(p2Cx, p2Cy, p2Cz, 1.0f);

    // calculating needed rotation around the 3 axes.
    float r = length(transformedOuterOld);
    float phi = atan2f(p2Cy, p2Cx);
    float theta = acos(p2Cz / r);

    mat4 wToS(exV.x, eyV.x, ezV.x, 0,
              exV.y, eyV.y, ezV.y, 0,
              exV.z, eyV.z, ezV.z, 0,
              0, 0, 0, 1
    );

    auto TransP = translate(glm::mat4(1.0f), vec3(-p1));
    mat4 transformationToLocal = wToS * TransP;

    // transformation inverted
    mat4 transformationFromLocal = inverse(transformationToLocal);

    // rotation
    float alpha = static_cast<float>(rotationAlpha * M_PI) / 180.0f;
    auto rotAlpha = rotate(mat4(1.0f), alpha, vec3(1, 0, 0));

    // rotation
    float rotThetaValue = static_cast<float>(-M_PI / 2.0) + theta;
    auto rotTheta = rotate(mat4(1.0f), rotThetaValue, vec3(0, 1, 0));

    // rotation
    auto rotPhi = rotate(mat4(1.0f), phi, vec3(0, 0, 1));

    // translation
    float magnitudeDif = length(p1 - p2) - length(p1s - p2s);
    vec4 directionTrans = normalize(p2s - p1s) * (-(magnitudeDif / 2));
    auto translation = translate(mat4(1.0f), vec3(t + (directionTrans)));

    return translation * transformationFromLocal * rotPhi * rotTheta * rotAlpha * transformationToLocal;
}