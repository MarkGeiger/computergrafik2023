#version 430

layout (location = 0) out vec4 fragColor;

uniform vec3 lightSource;

in vec4 vPos;
in vec3 vNormal;
in vec4 vColor;

void main()
{
    vec3 vertexToLight = normalize(lightSource - vec3(vPos));
    float diffuse = max(dot(vNormal, vertexToLight), 0.0f);
    float ambient = 0.1f;
    vec3 color = vec3(vColor) * (diffuse + ambient);
    fragColor = vec4(color, 1.0f);
}