#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <iostream>
#include <algorithm>

#include "rendering/Shader.h"
#include "rendering/TextRenderer.h"
#include "rendering/Window.h"
#include "rendering/Camera.h"
#include "rendering/Grid.h"
#include "rendering/CoordinateSystem.h"
#include "rendering/ObjectRenderer.h"

#include "model/FlapTransform.h"
#include "model/Cube.h"

using namespace windowConstants;

Window mainWindow;
Camera camera;
Grid floorGrid;

TextRenderer *textRenderer;
Shader *shader = nullptr;
Shader *shaderLight = nullptr;

int fps = 0;
int fpsCounter = 20;
double timeLastFrame = 0;

void loadContent()
{
    shader = new Shader("Basic.vert", "Basic.frag");
    shader->apply();

    shader->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shader->setUniformMatrix4fv("view", camera.getViewMatrix());
    shader->setUniformMatrix4fv("proj", camera.getProjectionMatrix());

    shaderLight = new Shader("light.vert", "light.frag");
    shaderLight->apply();

    shaderLight->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shaderLight->setUniformMatrix4fv("view", camera.getViewMatrix());
    shaderLight->setUniformMatrix4fv("proj", camera.getProjectionMatrix());
}

void drawTopMenu(double time)
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    fpsCounter--;
    if (fpsCounter == 0)
    {
        fpsCounter = 20;
        fps = static_cast<int>(1 / (time - timeLastFrame));
    }
    const float SCALEX = 2.0 / WINDOW_WIDTH;
    const float SCALEY = 2.0 / WINDOW_HEIGHT;
    textRenderer->renderText("FPS: " + std::to_string(fps), -1, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F1: Wireframe", -0.7, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F2: Show Grid", -0.3, 0.95, SCALEX / 2, SCALEY / 2);
    timeLastFrame = time;
}

void render(double time)
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader->apply();

    glm::mat4 trans = glm::mat4(1.0f);
    if (mainWindow.isShowGrid())
    {
        floorGrid.drawGrid();
    }

    if (mainWindow.isWireframesEnabled())
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    shaderLight->setUniform3fv("lightSource", glm::vec3(1.5, 1.5, 2));
    shaderLight->setUniform3fv("lightSource", camera.getCamPosition());

    Cube cube(0.8, 0.06, 2);
    cube.setShader(*shaderLight);

    auto p1 = glm::vec3(-0.4, 0.03, 1);
    auto p2 = glm::vec3(-0.4, 0.03, -1);
    auto p1d = glm::vec3(1.5, -0.3, 1);
    auto p2d = glm::vec3(1.5, 0.4, -1);
    auto flapTransform = FlapTransform::transform(
            p1, p2,
            p1 + p1d * static_cast<float>(std::max(0.0, sin(time / 2))),
            p2 + p2d * static_cast<float>(std::max(0.0, sin(time / 2))),
            80.0f * static_cast<float>(std::max(0.0, sin(time / 2))));

    cube.setTransform(flapTransform);

    cube.draw();

    shader->apply();

    if (mainWindow.isShowGrid())
    {
        shader->setUniformMatrix4fv("model", glm::mat4(1.0f));
        CoordinateSystem::drawCoordinateSystem();
    }

    glDisable(GL_DEPTH_TEST);
    drawTopMenu(time);
    glEnable(GL_DEPTH_TEST);
}

void mainLoop()
{
    auto startTime = static_cast<float>(glfwGetTime());
    double newTime;

    /* Loop until the user closes the window */
    while (mainWindow.startFrame())
    {
        newTime = static_cast<float>(glfwGetTime()) - startTime;

        render(newTime);

        mainWindow.endFrame();
    }
}

int main()
{
    mainWindow.init();
    loadContent();
    floorGrid.setShader(*shader);

    mainWindow.setShader(*shader, *shaderLight);
    mainWindow.setCamera(camera);

    textRenderer = new TextRenderer();

    // does not exit method until openGL window closed
    mainLoop();

    // cleanup
    glfwTerminate();
    delete shader;
    delete shaderLight;
    delete textRenderer;
    return 0;
}