#include "Window.h"

#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Constants.h"

void Window::windowSizeCallback(GLFWwindow *lwindow, int width, int height)
{
    glViewport(0, 0, width, height);
    if (m_camera != nullptr)
    {
        m_camera->makeNewProjectionMatrix(width, height);
    }

    if (m_shader != nullptr && m_shaderLight != nullptr)
    {
        m_shader->setUniformMatrix4fv("view", m_camera->getViewMatrix());
        m_shader->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
        m_shaderLight->setUniformMatrix4fv("view", m_camera->getViewMatrix());
        m_shaderLight->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
    }
}

void Window::keyCallback(GLFWwindow *lwindow, int key, int scancode, int action, int mods)
{
    if (glfwGetKey(lwindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(lwindow, true);
    }
    if (glfwGetKey(lwindow, GLFW_KEY_F1) == GLFW_PRESS)
    {
        wireframesEnabled = !wireframesEnabled;
    }
    if (glfwGetKey(lwindow, GLFW_KEY_F2) == GLFW_PRESS)
    {
        showGrid = !showGrid;
    }
}

void Window::cursorPositionCallback(GLFWwindow *lwindow, double xpos, double ypos)
{
    float rotY = static_cast<float>(transX - xpos) / 300.0f;
    float rotX = static_cast<float>(transY - ypos) / 300.0f;
    if (leftMousePressed)
    {
        glm::vec4 tempVec = glm::translate(glm::mat4(1.0f), m_camera->getCamLookAt()) *
                            glm::rotate(glm::mat4(1.0f), rotX, glm::vec3(1, 0, 0)) *
                            glm::rotate(glm::mat4(1.0f), rotY, glm::vec3(0, 1, 0)) *
                            glm::translate(glm::mat4(1.0f), -m_camera->getCamLookAt()) *
                            glm::vec4(m_camera->getCamPosition(), 1.0f);
        m_camera->setCamPosition(glm::vec3(tempVec.x, tempVec.y, tempVec.z));

        m_camera->setViewMatrix(
                glm::lookAt(m_camera->getCamPosition(), m_camera->getCamLookAt(), m_camera->getCamUp()));
        if (m_shader != nullptr && m_shaderLight != nullptr)
        {
            m_shader->setUniformMatrix4fv("view", m_camera->getViewMatrix());
            m_shader->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
            m_shaderLight->setUniformMatrix4fv("view", m_camera->getViewMatrix());
            m_shaderLight->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
        }
    } else if (rightMousePressed)
    {
        glm::vec3 viewDir = glm::normalize(m_camera->getCamPosition() - m_camera->getCamLookAt());
        glm::vec2 normalXZ = glm::vec2{-viewDir.z, viewDir.x};
        glm::vec2 normalYZ = glm::vec2{-viewDir.z, viewDir.y};

        float translateX = -normalXZ.x * rotY;
        float translateZ = -normalXZ.y * rotY - normalYZ.y * rotX;
        float translateY = -normalYZ.x * rotX;

        m_camera->setCamPosition(m_camera->getCamPosition() + glm::vec3(translateX, translateY, translateZ));
        m_camera->setCamLookAt(m_camera->getCamLookAt() + glm::vec3(translateX, translateY, translateZ));

        m_camera->setViewMatrix(
                glm::lookAt(m_camera->getCamPosition(), m_camera->getCamLookAt(), m_camera->getCamUp()));
        m_camera->setViewMatrix(
                glm::lookAt(m_camera->getCamPosition(), m_camera->getCamLookAt(), m_camera->getCamUp()));
        if (m_shader != nullptr && m_shaderLight != nullptr)
        {
            m_shader->setUniformMatrix4fv("view", m_camera->getViewMatrix());
            m_shader->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
            m_shaderLight->setUniformMatrix4fv("view", m_camera->getViewMatrix());
            m_shaderLight->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
        }
    }
    transX = xpos;
    transY = ypos;
}

void Window::mouseButtonCallback(GLFWwindow *lwindow, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
        leftMousePressed = true;
    }
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
        leftMousePressed = false;
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
    {
        rightMousePressed = true;
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
    {
        rightMousePressed = false;
    }
}

void Window::scrollCallback(GLFWwindow *lwindow, double xoffset, double yoffset)
{
    glm::vec3 viewDir = glm::normalize(m_camera->getCamLookAt() - m_camera->getCamPosition());
    m_camera->setCamPosition(m_camera->getCamPosition() + viewDir * 0.2f * static_cast<float>(yoffset));
    m_camera->setViewMatrix(
            glm::lookAt(m_camera->getCamPosition(), m_camera->getCamLookAt(), m_camera->getCamUp()));
    if (m_shader != nullptr && m_shaderLight != nullptr)
    {
        m_shader->setUniformMatrix4fv("view", m_camera->getViewMatrix());
        m_shader->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
        m_shaderLight->setUniformMatrix4fv("view", m_camera->getViewMatrix());
        m_shaderLight->setUniformMatrix4fv("proj", m_camera->getProjectionMatrix());
    }
}

void Window::init()
{
    /* Initialize the library */
    if (!glfwInit())
    {
        throw std::runtime_error("Could not initialize glfw");
    }

    /* Create a m_windowed mode window and its OpenGL context */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);

    m_window = glfwCreateWindow(windowConstants::WINDOW_WIDTH, windowConstants::WINDOW_HEIGHT, "Computergrafik",
                                nullptr, nullptr);

    if (!m_window)
    {
        glfwTerminate();
        throw std::runtime_error("Could not create m_window");
    }

    glfwMakeContextCurrent(m_window);
    glfwSetWindowUserPointer(m_window, this);

    auto funcSize = [](GLFWwindow *window, int w, int h)
    {
        static_cast<Window *>(glfwGetWindowUserPointer(window))->windowSizeCallback(window, w, h);
    };
    glfwSetWindowSizeCallback(m_window, funcSize);

    auto funcKey = [](GLFWwindow *window, int k, int s, int a, int m)
    {
        static_cast<Window *>(glfwGetWindowUserPointer(window))->keyCallback(window, k, s, a, m);
    };
    glfwSetKeyCallback(m_window, funcKey);

    auto funcPos = [](GLFWwindow *window, double x, double y)
    {
        static_cast<Window *>(glfwGetWindowUserPointer(window))->cursorPositionCallback(window, x, y);
    };
    glfwSetCursorPosCallback(m_window, funcPos);

    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    auto funcButton = [](GLFWwindow *window, int k, int s, int a)
    {
        static_cast<Window *>(glfwGetWindowUserPointer(window))->mouseButtonCallback(window, k, s, a);
    };
    glfwSetMouseButtonCallback(m_window, funcButton);

    glfwSetInputMode(m_window, GLFW_STICKY_MOUSE_BUTTONS, 1);
    auto funcScroll = [](GLFWwindow *window, double x, double y)
    {
        static_cast<Window *>(glfwGetWindowUserPointer(window))->scrollCallback(window, x, y);
    };
    glfwSetScrollCallback(m_window, funcScroll);

    /* Initialize glad */
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }

    glViewport(0, 0, windowConstants::WINDOW_WIDTH, windowConstants::WINDOW_HEIGHT);

    glEnable(GL_MULTISAMPLE);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthRange(0.1f, 10.0f);
}

bool Window::startFrame()
{
    return !glfwWindowShouldClose(m_window);
}

bool Window::endFrame()
{
    /* Swap front and back buffers */
    glfwSwapBuffers(m_window);

    /* Poll for and process events */
    glfwPollEvents();

    return true;
}

void Window::setShader(Shader &shader, Shader &shaderLight)
{
    m_shader = &shader;
    m_shaderLight = &shaderLight;
}

void Window::setCamera(Camera &camera)
{
    m_camera = &camera;
}

bool Window::isWireframesEnabled() const
{
    return wireframesEnabled;
}

bool Window::isShowGrid() const
{
    return showGrid;
}
