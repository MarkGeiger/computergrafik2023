#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "rendering/Shader.h"

GLFWwindow *window;
const int WINDOW_WIDTH = 1024;
const int WINDOW_HEIGHT = 768;

Shader *shader = nullptr;

/* Matrices */
glm::vec3 cam_position = glm::vec3(0.0f, 0.0f, 2.0f);
glm::vec3 cam_look_at = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 cam_up = glm::vec3(0.0f, 1.0f, 0.0f);

glm::mat4 model_matrix = glm::mat4(1.0f);
glm::mat4 view_matrix = glm::lookAt(cam_position, cam_look_at, cam_up);
glm::mat4 projection_matrix = glm::perspectiveFov(glm::radians(90.0f), float(WINDOW_WIDTH), float(WINDOW_HEIGHT), 0.1f,
                                                  10.0f);

void drawShape(const GLfloat *g_vertex_buffer_data, unsigned long size);

void window_size_callback(GLFWwindow *lwindow, int width, int height)
{
    glViewport(0, 0, width, height);
    projection_matrix = glm::perspectiveFov(glm::radians(90.0f), float(width), float(height), 0.1f, 10.0f);

    if (shader != nullptr)
    {
        shader->setUniformMatrix4fv("viewProj", projection_matrix * view_matrix);
    }
}

void init()
{
    /* Initialize the library */
    if (!glfwInit())
    {
        throw std::runtime_error("Could not initialize glfw");
    }

    /* Create a windowed mode window and its OpenGL context */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Computergrafik", nullptr, nullptr);

    if (!window)
    {
        glfwTerminate();
        throw std::runtime_error("Could not create window");
    }

    glfwMakeContextCurrent(window);
    glfwSetWindowSizeCallback(window, window_size_callback);

    /* Initialize glad */
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }

    /* Set the viewport */
    glClearColor(0.6784f, 0.8f, 1.0f, 1.0f);
    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    glEnable(GL_DEPTH_TEST);
}

void loadContent()
{
    /* Create and apply basic shader */
    shader = new Shader("Basic.vert", "Basic.frag");
    shader->apply();

    shader->setUniformMatrix4fv("world", model_matrix);
    shader->setUniformMatrix4fv("viewProj", projection_matrix * view_matrix);
    shader->setUniform3fv("cam_pos", cam_position);
}

void drawShape(const GLfloat *g_vertex_buffer_data, unsigned long size)
{
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, static_cast<long>(size), g_vertex_buffer_data, GL_STATIC_DRAW);

    // every time
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
            0,
            3, // size
            GL_FLOAT,// type
            GL_FALSE,// normalized?
            0, // stride
            (void *) nullptr // array buffer offset
    );
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); // Starting from vertex 0; 3 vertices total -> 1 triangle
    glDisableVertexAttribArray(0);
}

void render(float time)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    static const GLfloat g_vertex_buffer_data[] = {
            -0.0f, -0.0f, 0.0f,
            -0.0f, 1.0f, 0.0f,
            2.0f, 0.0f, 0.0f,
            2.0f, 1.0f, 0.0f,
    };

    drawShape(g_vertex_buffer_data, sizeof(g_vertex_buffer_data));

    // rotate world matrix
    // model_matrix = glm::rotate(glm::mat4(1.0f), time * glm::radians(-50.0f), glm::vec3(0, 1, 0));
    glm::mat4 ind(1.0f);
    model_matrix = ind;
    // translation: model_matrix = glm::translate(ind, glm::vec3(-0.5,0,0));

    model_matrix =
            glm::translate(ind, glm::vec3(0.5, 0.5, 0)) *
            glm::scale(ind, glm::vec3(0.5, 0.5, 0.5)) *
            glm::translate(ind, glm::vec3(-0.5, -0.5, 0));

    shader->setUniformMatrix4fv("world", model_matrix);
    shader->apply();
}

void update()
{
    auto startTime = static_cast<float>(glfwGetTime());
    float newTime;
    float gameTime;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Update game time value */
        newTime = static_cast<float>(glfwGetTime());
        gameTime = newTime - startTime;

        /* Render here */
        render(gameTime);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }
}

int main()
{
    init();
    loadContent();

    // does not exit method until openGL window closed
    update();

    // cleanup
    glfwTerminate();
    delete shader;

    return 0;
}