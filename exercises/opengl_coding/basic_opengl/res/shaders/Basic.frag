#version 430

in vec3 o_position;

layout(location = 0) out vec4 fragColor;

uniform vec3 cam_pos;

void main()
{
	vec3 frag_pos = normalize(cam_pos - o_position);
	fragColor = vec4(frag_pos[0],frag_pos[1],frag_pos[2], 1.0f);
}