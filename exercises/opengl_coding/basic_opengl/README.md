# Computergrafik Lecture

Computergrafik lecture exercises

## Prerequisites

```bash
  sudo apt update
  sudo apt install -y build-essential cmake mesa-common-dev mesa-utils freeglut3-dev libassimp-dev libglfw3-dev
```

## Build & run

```
  mkdir build
  cd build
  cmake ..
  make
  ./Computergrafik_1
```
