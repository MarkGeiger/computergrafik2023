#include "ObjectRenderer.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

using namespace ObjectRenderer;

void ObjectRenderer::drawShapePos(const GLfloat *g_vertex_buffer_data, unsigned long size, uint16_t type)
{
    // TODO: Aufgabe 2.1
}

void ObjectRenderer::drawShapePosColor(const GLfloat *g_vertex_buffer_data, unsigned long size, uint16_t type)
{
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, static_cast<long>(size), g_vertex_buffer_data, GL_STATIC_DRAW);

    // first attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) nullptr);
    glEnableVertexAttribArray(0);
    // second attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) (3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glDrawArrays(type, 0, static_cast<int>(size / sizeof(float) / 6));
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

