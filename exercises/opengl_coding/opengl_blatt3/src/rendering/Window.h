#pragma once

#define GLFW_INCLUDE_NONE

#include "GLFW/glfw3.h"

#include "Shader.h"
#include "Camera.h"

class Window
{

public:

    void init();

    void setShader(Shader &shader, Shader &shaderLight);
    void setCamera(Camera &camera);
    bool startFrame();
    bool endFrame();

    void windowSizeCallback(GLFWwindow *lwindow, int width, int height);
    void keyCallback(GLFWwindow *lwindow, int key, int scancode, int action, int mods);
    void cursorPositionCallback(GLFWwindow *lwindow, double xpos, double ypos);
    void mouseButtonCallback(GLFWwindow *lwindow, int button, int action, int mods);
    void scrollCallback(GLFWwindow *lwindow, double xoffset, double yoffset);

    [[nodiscard]] bool isWireframesEnabled() const;
    [[nodiscard]] bool isShowGrid() const;

private:
    GLFWwindow *m_window{nullptr};
    Shader *m_shader{nullptr};
    Shader *m_shaderLight{nullptr};
    Camera *m_camera{nullptr};

    bool wireframesEnabled = false;
    bool showGrid = true;

    bool leftMousePressed = false;
    bool rightMousePressed = false;
    double transX = 0;
    double transY = 0;
};