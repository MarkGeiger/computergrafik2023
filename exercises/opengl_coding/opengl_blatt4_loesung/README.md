# Computergrafik Lecture

Computergrafik lecture exercises

## Prerequisites

```bash
  sudo apt mainLoop
  sudo apt install -y build-essential cmake mesa-common-dev mesa-utils freeglut3-dev libassimp-dev libglfw3-dev libfreetype-dev
```

## Build & run

```
  mkdir build
  cd build
  cmake ..
  make
  ./Computergrafik_1
```
