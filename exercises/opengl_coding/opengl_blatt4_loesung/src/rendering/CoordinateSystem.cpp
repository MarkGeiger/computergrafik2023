#include "CoordinateSystem.h"
#include "ObjectRenderer.h"

void CoordinateSystem::drawCoordinateSystem()
{
    static const GLfloat g_vertex_buffer_data_coordinates[] = {
            0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 2.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            2.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 2.0f, 0.0f, 0.0f, 1.0f,
    };
    ObjectRenderer::drawShapePosColor(g_vertex_buffer_data_coordinates, sizeof(g_vertex_buffer_data_coordinates),
                                      GL_LINES);
}
