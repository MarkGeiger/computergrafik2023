#include "Cube.h"
#include "rendering/ObjectRenderer.h"

Cube::Cube(float w, float h, float d)
{
    glm::vec3 color(1.0, 0, 0);
    Rectangle front(w, h, glm::vec3(0, 0, d / 2.0), color);
    m_faces.push_back(front);
    Rectangle back(w, h, glm::vec3(0, 0, -d / 2.0), color);
    back.rotate(180, glm::vec3(0, 1, 0));
    m_faces.push_back(back);
    Rectangle sideL(d, h, glm::vec3(-w / 2, 0, 0), color);
    sideL.rotate(-90, glm::vec3(0, 1, 0));
    m_faces.push_back(sideL);
    Rectangle sideR(d, h, glm::vec3(w / 2, 0, 0), color);
    sideR.rotate(90, glm::vec3(0, 1, 0));
    m_faces.push_back(sideR);
    Rectangle top(w, d, glm::vec3(0, h / 2.0, 0), color);
    top.rotate(-90, glm::vec3(1, 0, 0));
    m_faces.push_back(top);
    Rectangle bottom(w, d, glm::vec3(0, -h / 2.0, 0), color);
    bottom.rotate(90, glm::vec3(1, 0, 0));
    m_faces.push_back(bottom);
    /*
    m_vertex_buffer_data_grid[0] = -1.f * w;
    m_vertex_buffer_data_grid[1] = 1.f * h;
    m_vertex_buffer_data_grid[2] = 1.f * d;

    m_vertex_buffer_data_grid[6] = 1.f * w;
    m_vertex_buffer_data_grid[7] = 1.f * h;
    m_vertex_buffer_data_grid[8] = 1.f * d;

    m_vertex_buffer_data_grid[6] = -1.f * w;
    m_vertex_buffer_data_grid[7] = -1.f * h;
    m_vertex_buffer_data_grid[8] = 1.f * d;

    m_vertex_buffer_data_grid[9] = 1.f * w;
    m_vertex_buffer_data_grid[10] = -1.f * h;
    m_vertex_buffer_data_grid[11] = 1.f * d;

    m_vertex_buffer_data_grid[12] = 1.f * w;
    m_vertex_buffer_data_grid[13] = -1.f * h;
    m_vertex_buffer_data_grid[14] = -1.f * d;

    m_vertex_buffer_data_grid[15] = 1.f * w;
    m_vertex_buffer_data_grid[16] = 1.f * h;
    m_vertex_buffer_data_grid[17] = 1.f * d;

    m_vertex_buffer_data_grid[18] = 1.f * w;
    m_vertex_buffer_data_grid[19] = 1.f * h;
    m_vertex_buffer_data_grid[20] = -1.f * d;

    m_vertex_buffer_data_grid[21] = -1.f * w;
    m_vertex_buffer_data_grid[22] = 1.f * h;
    m_vertex_buffer_data_grid[23] = 1.f * d;

    m_vertex_buffer_data_grid[24] = -1.f * w;
    m_vertex_buffer_data_grid[25] = 1.f * h;
    m_vertex_buffer_data_grid[26] = -1.f * d;

    m_vertex_buffer_data_grid[27] = -1.f * w;
    m_vertex_buffer_data_grid[28] = -1.f * h;
    m_vertex_buffer_data_grid[29] = 1.f * d;

    m_vertex_buffer_data_grid[30] = -1.f * w;
    m_vertex_buffer_data_grid[31] = -1.f * h;
    m_vertex_buffer_data_grid[32] = -1.f * d;

    m_vertex_buffer_data_grid[33] = 1.f * w;
    m_vertex_buffer_data_grid[34] = -1.f * h;
    m_vertex_buffer_data_grid[35] = -1.f * d;

    m_vertex_buffer_data_grid[36] = -1.f * w;
    m_vertex_buffer_data_grid[37] = 1.f * h;
    m_vertex_buffer_data_grid[38] = -1.f * d;

    m_vertex_buffer_data_grid[39] = 1.f * w;
    m_vertex_buffer_data_grid[40] = 1.f * h;
    m_vertex_buffer_data_grid[41] = -1.f * d;
    */
}

void Cube::setShader(Shader &shader)
{
    m_shader = &shader;
}

void Cube::draw()
{
    m_shader->apply();
    for (auto face: m_faces)
    {
        face.setTransform(m_transform * face.getTransform());
        face.draw(m_shader);
    }
    // ObjectRenderer::drawShapePos(m_vertex_buffer_data_grid, sizeof(m_vertex_buffer_data_grid), GL_TRIANGLE_STRIP);
}

void Cube::setTransform(glm::mat4 transform)
{
    m_transform = transform;
}

glm::mat4 Cube::getTransform()
{
    return m_transform;
}
