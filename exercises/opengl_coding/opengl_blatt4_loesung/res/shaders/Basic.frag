#version 430

layout(location = 0) out vec4 fragColor;

in vec3 vColor;
in vec4 pos;

void main()
{
    fragColor = vec4(vColor, 1.0f);
}