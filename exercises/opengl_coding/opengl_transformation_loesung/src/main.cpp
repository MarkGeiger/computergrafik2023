#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "rendering/Shader.h"
#include "rendering/TextRenderer.h"

#include "model/Rectangle.h"

bool wireframesEnabled = false;
bool showGrid = true;
GLFWwindow *window;
TextRenderer *textRenderer;

const int WINDOW_WIDTH = 1024;
const int WINDOW_HEIGHT = 768;
const float SCALEX = 2.0 / WINDOW_WIDTH;
const float SCALEY = 2.0 / WINDOW_HEIGHT;
double timeLastFrame = 0;
int fps = 0;
int fpsCounter = 20;

bool leftMousePressed = false;
bool rightMousePressed = false;
double transX = 0;
double transY = 0;

Shader *shader = nullptr;

glm::vec3 cam_position = glm::vec3(1.0f, 1.0f, 2.5f);
glm::vec3 cam_look_at = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 cam_up = glm::vec3(0.0f, 1.0f, 0.0f);

glm::mat4 model_matrix = glm::mat4(1.0f);
glm::mat4 view_matrix = glm::lookAt(cam_position, cam_look_at, cam_up);
glm::mat4 projection_matrix = glm::perspectiveFov(glm::radians(100.0f),
                                                  float(WINDOW_WIDTH),
                                                  float(WINDOW_HEIGHT),
                                                  0.1f,
                                                  10.0f);
glm::vec3 newCamPos = cam_position;
glm::vec3 newLookAtPos = cam_look_at;

float gridStep = 0.1f;
constexpr uint8_t gridSize = 40;
GLfloat g_vertex_buffer_data_grid[(gridSize * gridSize + 2) * 6 * 4] = {}; // 2 extra liens for border

static void windowSizeCallback(GLFWwindow *lwindow, int width, int height) {
    glViewport(0, 0, width, height);
    projection_matrix = glm::perspectiveFov(glm::radians(90.0f), float(width), float(height), 0.1f, 10.0f);

    if (shader != nullptr) {
        shader->setUniformMatrix4fv("view", view_matrix);
        shader->setUniformMatrix4fv("proj", projection_matrix);
    }
}

static void keyCallback(GLFWwindow *lwindow, int key, int scancode, int action, int mods) {
    if (glfwGetKey(lwindow, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(lwindow, true);
    }
    if (glfwGetKey(lwindow, GLFW_KEY_F1) == GLFW_PRESS) {
        wireframesEnabled = !wireframesEnabled;
    }
    if (glfwGetKey(lwindow, GLFW_KEY_F2) == GLFW_PRESS) {
        showGrid = !showGrid;
    }
}

static void cursorPositionCallback(GLFWwindow *lwindow, double xpos, double ypos) {
    float rotY = static_cast<float>(transX - xpos) / 300.0f;
    float rotX = static_cast<float>(transY - ypos) / 300.0f;
    if (leftMousePressed) {
        glm::vec4 tempVec = glm::translate(glm::mat4(1.0f), cam_look_at) *
                            glm::rotate(glm::mat4(1.0f), rotX, glm::vec3(1, 0, 0)) *
                            glm::rotate(glm::mat4(1.0f), rotY, glm::vec3(0, 1, 0)) *
                            glm::translate(glm::mat4(1.0f), -cam_look_at) *
                            glm::vec4(cam_position, 1.0f);
        newCamPos = glm::vec3(tempVec.x, tempVec.y, tempVec.z);

        view_matrix = glm::lookAt(newCamPos, newLookAtPos, cam_up);
        if (shader != nullptr) {
            shader->setUniformMatrix4fv("view", view_matrix);
            shader->setUniformMatrix4fv("proj", projection_matrix);
        }
    } else if (rightMousePressed) {
        glm::vec3 viewDir = glm::normalize(cam_position - cam_look_at);
        glm::vec2 normalXZ = glm::vec2{-viewDir.z, viewDir.x};
        glm::vec2 normalYZ = glm::vec2{-viewDir.z, viewDir.y};

        float translateX = -normalXZ.x * rotY;
        float translateZ = -normalXZ.y * rotY - normalYZ.y * rotX;
        float translateY = -normalYZ.x * rotX;

        newCamPos = cam_position + glm::vec3(translateX, translateY, translateZ);
        newLookAtPos = cam_look_at + glm::vec3(translateX, translateY, translateZ);

        view_matrix = glm::lookAt(newCamPos, newLookAtPos, cam_up);
        if (shader != nullptr) {
            shader->setUniformMatrix4fv("view", view_matrix);
            shader->setUniformMatrix4fv("proj", projection_matrix);
        }
    } else {
        cam_position = newCamPos;
        cam_look_at = newLookAtPos;
        transX = xpos;
        transY = ypos;
    }
}

void mouseButtonCallback(GLFWwindow *lwindow, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        leftMousePressed = true;
    }
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        leftMousePressed = false;
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
        rightMousePressed = true;
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE) {
        rightMousePressed = false;
    }
}

void scrollCallback(GLFWwindow *lwindow, double xoffset, double yoffset) {
    glm::vec3 viewDir = glm::normalize(cam_look_at - cam_position);
    cam_position = cam_position + viewDir * 0.2f * static_cast<float>(yoffset);
    newCamPos = cam_position;
    view_matrix = glm::lookAt(cam_position, cam_look_at, cam_up);
    if (shader != nullptr) {
        shader->setUniformMatrix4fv("view", view_matrix);
        shader->setUniformMatrix4fv("proj", projection_matrix);
    }
}

void fillGridData() {
    for (uint8_t i = 0; i < gridSize; i++) {
        for (uint8_t j = 0; j < gridSize; j++) {
            uint8_t sizeOfOneVertex = 6;
            int index = (i * gridSize + j) * sizeOfOneVertex * 4;
            g_vertex_buffer_data_grid[index] = static_cast<float>(j) * gridStep; // x
            g_vertex_buffer_data_grid[index + 1] = 0; // y
            g_vertex_buffer_data_grid[index + 2] = static_cast<float>(i) * gridStep; // z
            g_vertex_buffer_data_grid[index + 3] = 0.0f; // r
            g_vertex_buffer_data_grid[index + 4] = 0.8f; // g
            g_vertex_buffer_data_grid[index + 5] = 0.0f; // b

            g_vertex_buffer_data_grid[index + 6] = static_cast<float>(j + 1) * gridStep; // x
            g_vertex_buffer_data_grid[index + 7] = 0; // y
            g_vertex_buffer_data_grid[index + 8] = static_cast<float>(i) * gridStep; // z
            g_vertex_buffer_data_grid[index + 9] = 0.0f; // r
            g_vertex_buffer_data_grid[index + 10] = 0.8f; // g
            g_vertex_buffer_data_grid[index + 11] = 0.0f; // b

            g_vertex_buffer_data_grid[index + 12] = static_cast<float>(j + 1) * gridStep; // x
            g_vertex_buffer_data_grid[index + 13] = 0; // y
            g_vertex_buffer_data_grid[index + 14] = static_cast<float>(i) * gridStep; // z
            g_vertex_buffer_data_grid[index + 15] = 0.0f; // r
            g_vertex_buffer_data_grid[index + 16] = 0.8f; // g
            g_vertex_buffer_data_grid[index + 17] = 0.0f; // b

            g_vertex_buffer_data_grid[index + 18] = static_cast<float>(j + 1) * gridStep; // x
            g_vertex_buffer_data_grid[index + 19] = 0; // y
            g_vertex_buffer_data_grid[index + 20] = static_cast<float>(i + 1) * gridStep; // z
            g_vertex_buffer_data_grid[index + 21] = 0.0f; // r
            g_vertex_buffer_data_grid[index + 22] = 0.8f; // g
            g_vertex_buffer_data_grid[index + 23] = 0.0f; // b
        }
    }
    uint8_t sizeOfOneVertex = 6;
    int index = ((gridSize - 1) * gridSize + gridSize) * sizeOfOneVertex * 4;
    g_vertex_buffer_data_grid[index] = static_cast<float>(0) * gridStep; // x
    g_vertex_buffer_data_grid[index + 1] = 0; // y
    g_vertex_buffer_data_grid[index + 2] = static_cast<float>(0) * gridStep; // z
    g_vertex_buffer_data_grid[index + 3] = 0.0f; // r
    g_vertex_buffer_data_grid[index + 4] = 0.8f; // g
    g_vertex_buffer_data_grid[index + 5] = 0.0f; // b

    g_vertex_buffer_data_grid[index + 6] = static_cast<float>(0) * gridStep; // x
    g_vertex_buffer_data_grid[index + 7] = 0; // y
    g_vertex_buffer_data_grid[index + 8] = static_cast<float>(gridSize) * gridStep; // z
    g_vertex_buffer_data_grid[index + 9] = 0.0f; // r
    g_vertex_buffer_data_grid[index + 10] = 0.8f; // g
    g_vertex_buffer_data_grid[index + 11] = 0.0f; // b

    g_vertex_buffer_data_grid[index + 12] = static_cast<float>(0) * gridStep; // x
    g_vertex_buffer_data_grid[index + 13] = 0; // y
    g_vertex_buffer_data_grid[index + 14] = static_cast<float>(gridSize) * gridStep; // z
    g_vertex_buffer_data_grid[index + 15] = 0.0f; // r
    g_vertex_buffer_data_grid[index + 16] = 0.8f; // g
    g_vertex_buffer_data_grid[index + 17] = 0.0f; // b

    g_vertex_buffer_data_grid[index + 18] = static_cast<float>(gridSize) * gridStep; // x
    g_vertex_buffer_data_grid[index + 19] = 0; // y
    g_vertex_buffer_data_grid[index + 20] = static_cast<float>(gridSize) * gridStep; // z
    g_vertex_buffer_data_grid[index + 21] = 0.0f; // r
    g_vertex_buffer_data_grid[index + 22] = 0.8f; // g
    g_vertex_buffer_data_grid[index + 23] = 0.0f; // b
}

void init() {
    /* Initialize the library */
    if (!glfwInit()) {
        throw std::runtime_error("Could not initialize glfw");
    }

    /* Create a windowed mode window and its OpenGL context */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);

    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Computergrafik", nullptr, nullptr);

    if (!window) {
        glfwTerminate();
        throw std::runtime_error("Could not create window");
    }

    glfwMakeContextCurrent(window);

    glfwSetWindowSizeCallback(window, windowSizeCallback);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, 1);
    glfwSetScrollCallback(window, scrollCallback);

    /* Initialize glad */
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }

    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    glEnable(GL_MULTISAMPLE);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthRange(0.1f, 10.0f);

    fillGridData();
}

void loadContent() {
    /* Create and apply basic shader */
    shader = new Shader("Basic.vert", "Basic.frag");
    shader->apply();

    shader->setUniformMatrix4fv("model", model_matrix);
    shader->setUniformMatrix4fv("view", view_matrix);
    shader->setUniformMatrix4fv("proj", projection_matrix);
}

void drawShape(const GLfloat *g_vertex_buffer_data, unsigned long size, uint16_t type) {
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, static_cast<long>(size), g_vertex_buffer_data, GL_STATIC_DRAW);

    // first attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) nullptr);
    glEnableVertexAttribArray(0);
    // second attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) (3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glDrawArrays(type, 0, static_cast<int>(size / sizeof(float) / 6));
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void drawTopMenu(double time) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    fpsCounter--;
    if (fpsCounter == 0) {
        fpsCounter = 20;
        fps = static_cast<int>(1 / (time - timeLastFrame));
    }
    textRenderer->renderText("FPS: " + std::to_string(fps), -1, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F1: Wireframe", -0.7, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F2: Show Grid", -0.3, 0.95, SCALEX / 2, SCALEY / 2);
    timeLastFrame = time;
}

static const glm::vec3 rectangleCenter{0.0f, 1.0f, 0.0f};
Rectangle rectangle(1.0f,1.0f, rectangleCenter);
void render(double time) {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 trans = glm::mat4(1.0f);
    if (showGrid) {
        trans = glm::translate(trans, glm::vec3(-gridStep * gridSize * 0.5, -0.01, -gridStep * gridSize * 0.5));
        shader->setUniformMatrix4fv("model", trans);
        drawShape(g_vertex_buffer_data_grid, sizeof(g_vertex_buffer_data_grid), GL_LINES);
    }

    if (wireframesEnabled) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    rectangle.rotate(-1.0f, glm::vec3{1,0,0});
    rectangle.draw(shader);

    auto vertex1 = rectangle.transformRectangleToWorldCoordinates(glm::vec2(0,0));
    auto vertex2 = rectangle.transformRectangleToWorldCoordinates(glm::vec2(0.75,0.5));
    auto vertex3 = rectangle.transformRectangleToWorldCoordinates(glm::vec2(0.9,0));
    auto vertex4 = rectangle.transformRectangleToWorldCoordinates(glm::vec2(0.9,1));
    GLfloat test[] =
    {
            vertex1.x, vertex1.y, vertex1.z, 0.0f , 1.0f, 0.0f,
            vertex2.x, vertex2.y, vertex2.z, 0.0f , 1.0f, 0.0f,
            vertex3.x, vertex3.y, vertex3.z, 0.0f , 1.0f, 0.0f,
            vertex4.x, vertex4.y, vertex4.z, 0.0f , 1.0f, 0.0f,
    };
    shader->setUniformMatrix4fv("model", glm::mat4(1.0f));
    drawShape(test, sizeof(test), GL_LINES);

    // draw our rectangle
    static const GLfloat g_vertex_buffer_data_coordinates[] = {
            0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 2.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            2.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 2.0f, 0.0f, 1.0f, 1.0f,
    };
    drawShape(g_vertex_buffer_data_coordinates, sizeof(g_vertex_buffer_data_coordinates), GL_LINES);

    glDisable(GL_DEPTH_TEST);
    drawTopMenu(time);
    glEnable(GL_DEPTH_TEST);

    shader->apply();
}

void update() {
    auto startTime = static_cast<float>(glfwGetTime());
    double newTime;
    double gameTime;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        /* Update game time value */
        newTime = static_cast<float>(glfwGetTime());
        gameTime = newTime - startTime;

        /* Render here */
        render(gameTime);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }
}

int main() {
    init();
    loadContent();

    textRenderer = new TextRenderer();
    // does not exit method until openGL window closed
    update();

    // cleanup
    glfwTerminate();
    delete shader;
    delete textRenderer;

    return 0;
}