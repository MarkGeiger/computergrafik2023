#include "TextRenderer.h"
#include "helpers/RootDir.h"

const char *VERTEX_SHADER = ""
                            "#version 410 core\n"
                            "in vec4 in_Position;\n"
                            "out vec2 texCoords;\n"
                            "void main(void) {\n"
                            "    gl_Position = vec4(in_Position.xy, 0, 1);\n"
                            "    texCoords = in_Position.zw;\n"
                            "}\n";


const char *FRAGMENT_SHADER = ""
                              "#version 410 core\n"
                              "precision highp float;\n"
                              "uniform sampler2D tex;\n"
                              "uniform vec4 color;\n"
                              "in vec2 texCoords;\n"
                              "out vec4 fragColor;\n"
                              "void main(void) {\n"
                              "    fragColor = vec4(1, 1, 1, texture(tex, texCoords).r) * color;\n"
                              "}\n";

TextRenderer::TextRenderer() {
    // Initialize our texture and VBOs
    glGenBuffers(1, &vbo);
    glGenVertexArrays(1, &vao);
    glGenTextures(1, &texture);
    glGenSamplers(1, &sampler);
    glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Initialize shader
    vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &VERTEX_SHADER, 0);
    glCompileShader(vs);

    fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &FRAGMENT_SHADER, 0);
    glCompileShader(fs);

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    // Set some initialize GL state
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.1, 0.2, 0.4, 0);

    // Get shader uniforms
    glUseProgram(program);
    glBindAttribLocation(program, 0, "in_Position");

    // Initialize and load our freetype face
    if (FT_Init_FreeType(&ft_lib) != 0) {
        throw std::exception();
    }

    if (FT_New_Face(ft_lib, ROOT_DIR "res/UbuntuMono-B.ttf", 0, &face) != 0) {
        throw std::exception();
    }

    // Create a GLFW window
    if (glfwInit() != GL_TRUE) {
        throw std::exception();
    }
}

TextRenderer::~TextRenderer() {
    FT_Done_Face(face);
    FT_Done_FreeType(ft_lib);
    glDeleteTextures(1, &texture);
    glDeleteSamplers(1, &sampler);
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
    glDeleteShader(vs);
    glDeleteShader(fs);
    glDeleteProgram(program);
}

void TextRenderer::renderText(const std::string &str, float x, float y, float sx, float sy) {

    GLuint texUniform = glGetUniformLocation(program, "tex");
    GLuint colorUniform = glGetUniformLocation(program, "color");

    // Bind stuff
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glBindSampler(0, sampler);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glUseProgram(program);
    glUniform4f(colorUniform, 1, 1, 1, 1);
    glUniform1i(texUniform, 0);

    FT_Set_Pixel_Sizes(face, 0, 50);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    const FT_GlyphSlot glyph = face->glyph;

    for (auto c: str) {
        if (FT_Load_Char(face, c, FT_LOAD_RENDER) != 0)
            continue;

        glTexImage2D(GL_TEXTURE_2D, 0, GL_R8,
                     glyph->bitmap.width, glyph->bitmap.rows,
                     0, GL_RED, GL_UNSIGNED_BYTE, glyph->bitmap.buffer);

        const float vx = x + glyph->bitmap_left * sx;
        const float vy = y + glyph->bitmap_top * sy;
        const float w = glyph->bitmap.width * sx;
        const float h = glyph->bitmap.rows * sy;

        struct {
            float x, y, s, t;
        } data[6] = {
                {vx,     vy,     0, 0},
                {vx,     vy - h, 0, 1},
                {vx + w, vy,     1, 0},
                {vx + w, vy,     1, 0},
                {vx,     vy - h, 0, 1},
                {vx + w, vy - h, 1, 1}
        };

        glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), data, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        x += (glyph->advance.x >> 6) * sx;
        y += (glyph->advance.y >> 6) * sy;
    }

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
}
