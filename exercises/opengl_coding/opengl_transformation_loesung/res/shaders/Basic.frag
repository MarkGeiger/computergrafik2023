#version 430

layout(location = 0) out vec4 fragColor;

in vec3 vColor;

// The entry point for our fragment shader.
void main()
{
    fragColor = vec4(vColor, 1.0f);
}