#include "TrimmedBezierSurface.h"
#include <vector>
#include "gtc/matrix_transform.hpp"
#include "rendering/ObjectRenderer.h"

TrimmedBezierSurface::TrimmedBezierSurface(const BiCubicBezierSurface<glm::vec3> &surface,
                                           const std::vector<std::vector<CubicBezierCurve<glm::vec2>>> &trimCurve)
        : m_surface(surface),
          m_trimCurves((trimCurve)) {
}

bool isVertexInsideLoop(glm::vec2 point, const std::vector<glm::vec2>& loop)
{
    int intersectionsRight = 0;
    int intersectionsLeft = 0;
    for (int i = 0; i < loop.size() - 1; i++)
    {
        auto p1 = loop[i];
        auto p2 = loop[i + 1];

        // 1: y = mx + b
        // 2: y = mx + b
        // ---> gerade

        if ((p1.y < point.y && p2.y >= point.y) || (p1.y >= point.y && p2.y < point.y))
        {
            float t = (point.y - p1.y) / (p2.y - p1.y);
            float x = p1.x + t * (p2.x - p1.x);
            auto intersection = glm::vec2(x, point.y);
            if (intersection.x > point.x)
            {
                intersectionsRight++;
            } else {
                intersectionsLeft++;
            }
        }
    }
    return intersectionsRight % 2 == 1 && intersectionsLeft % 2 == 1;
}

void TrimmedBezierSurface::draw(Shader &shaderSurface, Shader &shaderCurve) {
    float stepSize = 0.125f / 4.0f;

    shaderCurve.apply();
    //e.setUniformMatrix4fv("model", glm::mat4(1.0f));
    shaderCurve.setUniformMatrix4fv("model", glm::mat4(1.0f) *
                                             glm::rotate(glm::mat4(1.0f), glm::radians(30.0f), glm::vec3(1, 0, 0)));

    std::vector<std::vector<glm::vec2>> trimLoopVertices;
    for (const auto &loop: m_trimCurves)
    {
        std::vector<glm::vec2> loopVertices;
        for (int j = 0; j < loop.size(); j++)
        {
            auto curveSegment = loop.at(j);
            std::vector<GLfloat> bufferCurve;
            for (float t = 0; t <= 1.0f - stepSize; t = t + stepSize) {
                auto p1 = curveSegment.eval(t);
                auto p2 = curveSegment.eval(t + stepSize);

                loopVertices.emplace_back(p1);

                auto e1 = m_surface.eval(p1.x, p1.y);
                auto e2 = m_surface.eval(p2.x, p2.y);

                bufferCurve.emplace_back(e1.first.x);
                bufferCurve.emplace_back(e1.first.y);
                bufferCurve.emplace_back(e1.first.z);
                bufferCurve.emplace_back(0.0f);
                bufferCurve.emplace_back(0);
                bufferCurve.emplace_back(1.0f);
                bufferCurve.emplace_back(e2.first.x);
                bufferCurve.emplace_back(e2.first.y);
                bufferCurve.emplace_back(e2.first.z);
                bufferCurve.emplace_back(0.0f);
                bufferCurve.emplace_back(0);
                bufferCurve.emplace_back(1.0f);
            }

            if (j == loop.size() - 1)
            {
                auto p1 = curveSegment.eval(1.0f);
                loopVertices.emplace_back(p1);
            }
            ObjectRenderer::drawShapePosColor(&bufferCurve.front(), bufferCurve.size() * sizeof(GLfloat), GL_LINES);
        }
        trimLoopVertices.emplace_back(loopVertices);
    }


    shaderSurface.apply();
    shaderSurface.setUniformMatrix4fv("model", glm::mat4(1.0f) *
                                               glm::rotate(glm::mat4(1.0f), glm::radians(30.0f), glm::vec3(1, 0, 0)));
    std::vector<GLfloat> buffer;
    for (float i = 0; i <= 1.0f - stepSize; i = i + stepSize) {
        for (float t = 0; t <= 1.0f - stepSize; t = t + stepSize) {
            auto v1 = m_surface.eval(i, t);
            auto v2 = m_surface.eval(i, t + stepSize);
            auto v3 = m_surface.eval(i + stepSize, t);
            auto v4 = m_surface.eval(i + stepSize, t + stepSize);

            bool v1Trimmed = false;
            bool v2Trimmed = false;
            bool v3Trimmed = false;
            bool v4Trimmed = false;
            for (const auto& loop : trimLoopVertices)
            {
                v1Trimmed = v1Trimmed || isVertexInsideLoop(glm::vec2(i, t), loop);
                v2Trimmed = v2Trimmed || isVertexInsideLoop(glm::vec2(i, t + stepSize), loop);
                v3Trimmed = v3Trimmed || isVertexInsideLoop(glm::vec2(i + stepSize, t), loop);
                v4Trimmed = v4Trimmed || isVertexInsideLoop(glm::vec2(i +stepSize, t + stepSize), loop);
            }

            bool draw = !v1Trimmed && !v2Trimmed && !v3Trimmed && !v4Trimmed;

            if (draw) {
                // t1
                buffer.emplace_back(v1.first.x);
                buffer.emplace_back(v1.first.y);
                buffer.emplace_back(v1.first.z);
                buffer.emplace_back(1.0f);
                buffer.emplace_back(0);
                buffer.emplace_back(0);
                buffer.emplace_back(v1.second.x);
                buffer.emplace_back(v1.second.y);
                buffer.emplace_back(v1.second.z);
                buffer.emplace_back(v2.first.x);
                buffer.emplace_back(v2.first.y);
                buffer.emplace_back(v2.first.z);
                buffer.emplace_back(1.0f);
                buffer.emplace_back(0);
                buffer.emplace_back(0);
                buffer.emplace_back(v2.second.x);
                buffer.emplace_back(v2.second.y);
                buffer.emplace_back(v2.second.z);
                buffer.emplace_back(v3.first.x);
                buffer.emplace_back(v3.first.y);
                buffer.emplace_back(v3.first.z);
                buffer.emplace_back(1.0f);
                buffer.emplace_back(0);
                buffer.emplace_back(0);
                buffer.emplace_back(v3.second.x);
                buffer.emplace_back(v3.second.y);
                buffer.emplace_back(v3.second.z);

                // t2
                buffer.emplace_back(v4.first.x);
                buffer.emplace_back(v4.first.y);
                buffer.emplace_back(v4.first.z);
                buffer.emplace_back(1.0f);
                buffer.emplace_back(0);
                buffer.emplace_back(0);
                buffer.emplace_back(v4.second.x);
                buffer.emplace_back(v4.second.y);
                buffer.emplace_back(v4.second.z);
                buffer.emplace_back(v2.first.x);
                buffer.emplace_back(v2.first.y);
                buffer.emplace_back(v2.first.z);
                buffer.emplace_back(1.0f);
                buffer.emplace_back(0);
                buffer.emplace_back(0);
                buffer.emplace_back(v2.second.x);
                buffer.emplace_back(v2.second.y);
                buffer.emplace_back(v2.second.z);
                buffer.emplace_back(v3.first.x);
                buffer.emplace_back(v3.first.y);
                buffer.emplace_back(v3.first.z);
                buffer.emplace_back(1.0f);
                buffer.emplace_back(0);
                buffer.emplace_back(0);
                buffer.emplace_back(v3.second.x);
                buffer.emplace_back(v3.second.y);
                buffer.emplace_back(v3.second.z);
            }
        }
    }
    ObjectRenderer::drawShapePosColorNormal(&buffer.front(), buffer.size() * sizeof(GLfloat), GL_TRIANGLES);

}
