#pragma once

#include <vector>
#include "rendering/Shader.h"
#include "Rectangle.h"

class Cube
{
public:
    // TODO: Aufgabe 2.3
    Cube(float w, float h, float d);

    void setShader(Shader &shader);

    void draw();

    void setTransform(glm::mat4 transform);

    glm::mat4 getTransform();

private:
    // GLfloat m_vertex_buffer_data_grid[42] = {};
    glm::mat4 m_transform{1.0f};
    std::vector<Rectangle> m_faces;
    Shader * m_shader;
};
