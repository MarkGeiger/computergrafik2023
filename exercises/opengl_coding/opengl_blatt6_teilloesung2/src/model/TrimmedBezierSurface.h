#pragma once

#include "BiCubicBezierSurface.h"
#include "CubicBezierCurve.h"
#include "rendering/Shader.h"
#include <vector>

#include <glm/glm.hpp>

class TrimmedBezierSurface {

public:

    TrimmedBezierSurface(const BiCubicBezierSurface<glm::vec3> &surface,
                         const std::vector<std::vector<CubicBezierCurve<glm::vec2>>> &trimCurve);

    void draw(Shader &shaderSurface, Shader &shaderCurve);

private:
    BiCubicBezierSurface<glm::vec3> m_surface;
    std::vector<std::vector<CubicBezierCurve<glm::vec2>>> m_trimCurves;
};
