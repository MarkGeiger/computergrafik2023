
#include "Rectangle.h"
#include "rendering/Shader.h"
#include "rendering/ObjectRenderer.h"

Rectangle::Rectangle(float width, float heigth, glm::vec3 center, glm::vec3 color) :
        m_width(width), m_heigth(heigth), m_center(center), m_tras(glm::mat4(1.0f))
{
    auto n = getNormal();
    m_vertex_buffer_data[0] = -m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[1] = -m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[2] = m_center.z;
    m_vertex_buffer_data[3] = color.r;
    m_vertex_buffer_data[4] = color.g;
    m_vertex_buffer_data[5] = color.b;
    m_vertex_buffer_data[6] = n.x;
    m_vertex_buffer_data[7] = n.y;
    m_vertex_buffer_data[8] = n.z;

    m_vertex_buffer_data[9] = -m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[10] = m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[11] = m_center.z;
    m_vertex_buffer_data[12] = color.r;
    m_vertex_buffer_data[13] = color.g;
    m_vertex_buffer_data[14] = color.b;
    m_vertex_buffer_data[15] = n.x;
    m_vertex_buffer_data[16] = n.y;
    m_vertex_buffer_data[17] = n.z;

    m_vertex_buffer_data[18] = m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[19] = -m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[20] = m_center.z;
    m_vertex_buffer_data[21] = color.r;
    m_vertex_buffer_data[22] = color.g;
    m_vertex_buffer_data[23] = color.b;
    m_vertex_buffer_data[24] = n.x;
    m_vertex_buffer_data[25] = n.y;
    m_vertex_buffer_data[26] = n.z;

    m_vertex_buffer_data[27] = m_width / 2.0f + m_center.x;
    m_vertex_buffer_data[28] = m_heigth / 2 + m_center.y;
    m_vertex_buffer_data[29] = m_center.z;
    m_vertex_buffer_data[30] = color.r;
    m_vertex_buffer_data[31] = color.g;
    m_vertex_buffer_data[32] = color.b;
    m_vertex_buffer_data[33] = n.x;
    m_vertex_buffer_data[34] = n.y;
    m_vertex_buffer_data[35] = n.z;
}

void Rectangle::draw(Shader *pShader)
{
    updateNormals();
    pShader->setUniformMatrix4fv("model", m_tras);
    ObjectRenderer::drawShapePosColorNormal(m_vertex_buffer_data, sizeof(m_vertex_buffer_data), GL_TRIANGLE_STRIP);
}

void Rectangle::rotate(float angle, glm::vec3 rotateAround)
{
    auto tras = m_tras;
    tras = glm::translate(tras, m_center);
    tras = glm::rotate(tras, glm::radians(angle), rotateAround);
    tras = glm::translate(tras, -m_center);
    m_tras = tras;
}

glm::vec3 Rectangle::transformRectangleToWorldCoordinates(glm::vec2 cubeCoordinates)
{
    float startX = -m_width / 2.0f + m_center.x;
    float startY = m_heigth / 2.0f + m_center.y;
    return m_tras *
           glm::vec4(startX + cubeCoordinates.x * m_width, startY - cubeCoordinates.y * m_heigth, m_center.z, 1.0f);
}

glm::vec3 Rectangle::getNormal()
{
    // TODO: very simple approach, direction of normal not determined, should implement clock winding algorithm
    glm::vec3 v1 = transformRectangleToWorldCoordinates(glm::vec2(0, 0));
    glm::vec3 v2 = transformRectangleToWorldCoordinates(glm::vec2(1, 0));
    glm::vec3 v3 = transformRectangleToWorldCoordinates(glm::vec2(1, 1));
    auto v1v2 = glm::normalize(v2 - v1);
    auto v3v2 = glm::normalize(v2 - v3);
    return glm::normalize(glm::cross(v1v2, v3v2));
}

void Rectangle::updateNormals()
{
    auto n = getNormal();
    m_vertex_buffer_data[6] = n.x;
    m_vertex_buffer_data[7] = n.y;
    m_vertex_buffer_data[8] = n.z;

    m_vertex_buffer_data[15] = n.x;
    m_vertex_buffer_data[16] = n.y;
    m_vertex_buffer_data[17] = n.z;

    m_vertex_buffer_data[24] = n.x;
    m_vertex_buffer_data[25] = n.y;
    m_vertex_buffer_data[26] = n.z;

    m_vertex_buffer_data[33] = n.x;
    m_vertex_buffer_data[34] = n.y;
    m_vertex_buffer_data[35] = n.z;
}
glm::mat4 Rectangle::getTransform()
{
    return m_tras;
}
void Rectangle::setTransform(glm::mat4 transform)
{
    m_tras = transform;
}
