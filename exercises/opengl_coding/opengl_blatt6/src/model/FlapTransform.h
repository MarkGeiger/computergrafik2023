#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class FlapTransform
{
public:
    static glm::mat4 transform(glm::vec3 hingePoint1, glm::vec3 hingePoint2,
                               glm::vec3 hingePoint1s, glm::vec3 hingePoint2s,
                               float rotationAlpha);
};
