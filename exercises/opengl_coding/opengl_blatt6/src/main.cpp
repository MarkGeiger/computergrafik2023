#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <iostream>
#include <algorithm>
#include <utility>

#include "rendering/Shader.h"
#include "rendering/TextRenderer.h"
#include "rendering/Window.h"
#include "rendering/Camera.h"
#include "rendering/Grid.h"
#include "rendering/CoordinateSystem.h"
#include "rendering/ObjectRenderer.h"

#include "model/FlapTransform.h"
#include "model/Cube.h"

#include "model/BiCubicBezierSurface.h"

using namespace windowConstants;

Window mainWindow;
Camera camera;
Grid floorGrid;

TextRenderer *textRenderer;
Shader *shader = nullptr;
Shader *shaderLight = nullptr;

int fps = 0;
int fpsCounter = 20;
double timeLastFrame = 0;

void loadContent()
{
    shader = new Shader("Basic.vert", "Basic.frag");
    shader->apply();

    shader->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shader->setUniformMatrix4fv("view", camera.getViewMatrix());
    shader->setUniformMatrix4fv("proj", camera.getProjectionMatrix());

    shaderLight = new Shader("light.vert", "light.frag");
    shaderLight->apply();

    shaderLight->setUniformMatrix4fv("model", glm::mat4(1.0f));
    shaderLight->setUniformMatrix4fv("view", camera.getViewMatrix());
    shaderLight->setUniformMatrix4fv("proj", camera.getProjectionMatrix());
}

void drawTopMenu(double time)
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    fpsCounter--;
    if (fpsCounter == 0)
    {
        fpsCounter = 20;
        fps = static_cast<int>(1 / (time - timeLastFrame));
    }
    const float SCALEX = 2.0 / WINDOW_WIDTH;
    const float SCALEY = 2.0 / WINDOW_HEIGHT;
    textRenderer->renderText("FPS: " + std::to_string(fps), -1, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F1: Wireframe", -0.7, 0.95, SCALEX / 2, SCALEY / 2);
    textRenderer->renderText("F2: Show Grid", -0.3, 0.95, SCALEX / 2, SCALEY / 2);
    timeLastFrame = time;
}

void render(double time)
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader->apply();

    glm::mat4 trans = glm::mat4(1.0f);
    if (mainWindow.isShowGrid())
    {
        floorGrid.drawGrid();
    }

    if (mainWindow.isWireframesEnabled())
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    shaderLight->apply();
    shaderLight->setUniform3fv("lightSource", glm::vec3(1.5,1,4));

    BiCubicBezierSurface<glm::vec3> surface(
            glm::vec3(0, glm::sin(time), 0), glm::vec3(1, glm::sin(time + 2), 0), glm::vec3(2, 1, 0), glm::vec3(3, glm::sin(time + 6), 0),
            glm::vec3(0, glm::sin(time), 1), glm::vec3(1, 1, 1), glm::vec3(2, 1 + 2 * glm::sin(time + 4), 1), glm::vec3(3, glm::sin(time + 6), 1),
            glm::vec3(0, glm::sin(time), 2), glm::vec3(1, 1, 2), glm::vec3(2, 1 + 2 * glm::sin(time + 4), 2), glm::vec3(3, glm::sin(time +6), 2),
            glm::vec3(0, glm::sin(time), 3), glm::vec3(1, 1, 3), glm::vec3(2, 1, 3), glm::vec3(3, glm::sin(time + 6), 3)
    );

    shaderLight->setUniformMatrix4fv("model", glm::mat4(1.0f)*glm::rotate(glm::mat4(1.0f), glm::radians(30.0f), glm::vec3(1,0,0)));
    float stepSize = 0.125f/4.0f;
    std::vector<GLfloat> buffer;
    for (float i = 0; i <= 1.0f - stepSize; i = i + stepSize)
    {
        for (float t = 0; t <= 1.0f - stepSize; t = t + stepSize)
        {
            auto v1 = surface.eval(i, t);
            auto v2 = surface.eval(i, t + stepSize);
            auto v3 = surface.eval(i + stepSize, t);
            auto v4 = surface.eval(i + stepSize, t + stepSize);
            // t1
            buffer.emplace_back(v1.first.x);
            buffer.emplace_back(v1.first.y);
            buffer.emplace_back(v1.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v1.second.x);
            buffer.emplace_back(v1.second.y);
            buffer.emplace_back(v1.second.z);
            buffer.emplace_back(v2.first.x);
            buffer.emplace_back(v2.first.y);
            buffer.emplace_back(v2.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v2.second.x);
            buffer.emplace_back(v2.second.y);
            buffer.emplace_back(v2.second.z);
            buffer.emplace_back(v3.first.x);
            buffer.emplace_back(v3.first.y);
            buffer.emplace_back(v3.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v3.second.x);
            buffer.emplace_back(v3.second.y);
            buffer.emplace_back(v3.second.z);

            // t2
            buffer.emplace_back(v4.first.x);
            buffer.emplace_back(v4.first.y);
            buffer.emplace_back(v4.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v4.second.x);
            buffer.emplace_back(v4.second.y);
            buffer.emplace_back(v4.second.z);
            buffer.emplace_back(v2.first.x);
            buffer.emplace_back(v2.first.y);
            buffer.emplace_back(v2.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v2.second.x);
            buffer.emplace_back(v2.second.y);
            buffer.emplace_back(v2.second.z);
            buffer.emplace_back(v3.first.x);
            buffer.emplace_back(v3.first.y);
            buffer.emplace_back(v3.first.z);
            buffer.emplace_back(1.0f);
            buffer.emplace_back(0);
            buffer.emplace_back(0);
            buffer.emplace_back(v3.second.x);
            buffer.emplace_back(v3.second.y);
            buffer.emplace_back(v3.second.z);
        }
    }
    ObjectRenderer::drawShapePosColorNormal(&buffer.front(), buffer.size() * sizeof(GLfloat), GL_TRIANGLES);

    shader->apply();
    if (mainWindow.isShowGrid())
    {
        shader->setUniformMatrix4fv("model", glm::mat4(1.0f));
        CoordinateSystem::drawCoordinateSystem();
    }

    glDisable(GL_DEPTH_TEST);
    drawTopMenu(time);
    glEnable(GL_DEPTH_TEST);
}

void mainLoop()
{
    auto startTime = static_cast<float>(glfwGetTime());
    double newTime;

    /* Loop until the user closes the window */
    while (mainWindow.startFrame())
    {
        newTime = static_cast<float>(glfwGetTime()) - startTime;

        render(newTime);

        mainWindow.endFrame();
    }
}

int main()
{
    mainWindow.init();
    loadContent();
    floorGrid.setShader(*shader);

    mainWindow.setShader(*shader, *shaderLight);
    mainWindow.setCamera(camera);

    textRenderer = new TextRenderer();

    // does not exit method until openGL window closed
    mainLoop();

    // cleanup
    glfwTerminate();
    delete shader;
    delete shaderLight;
    delete textRenderer;
    return 0;
}